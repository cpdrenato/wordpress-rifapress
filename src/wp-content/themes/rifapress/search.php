<?php get_header(); ?>
<?php global $wp_query; ?>
<?php
$rifapressQtd  = ( $wp_query->found_posts > 1 ? __('Rifas', 'rifapress') : __('Rifa', 'rifapress'));
$resulNumber = $wp_query->found_posts;
?>
<div class="main_result">
<div class="container border-bottom my-4 shadow-sm">
    <div class="row">
	<div class="col-12 col-md-8 col-lg-9 my-2">
        <h2 class="text-center text-uppercase h4"><?php
		echo '<span class="result_number align-middle"> ' . number_format($resulNumber, 0, ".", ".");
		''
		?></span> <span class="text_result align-middle">  <?php echo $rifapressQtd; ?></span> </h2>
		</div>
		<div class="col-12 col-md-4 col-lg-3 my-2">
		<?php rifapress_sort_order(); ?>
		</div>
    </div>
	</div>
<div class="container">
<div class="row mb-5">
  <a class="btn btn-light btn-lg btn-block d-lg-none mb-4 text-uppercase" id="filter_action" href="#" role="button">Filtros</a>
<div class="col-12 d-none d-lg-block col-lg-4 mb-3 p-md-0 pr-md-3 filter">
<?php get_template_part('advanced-search-filter'); ?>
</div>
<div class="col-12 col-lg-8 p-md-0">
<div class="filter_result">
<?php list_filter();?>
</div>
	<?php
	if (have_posts()): while (have_posts()) : the_post();

	get_template_part('template-parts/content', get_post_format());

	endwhile;
	pagination();
	else:
	get_template_part('template-parts/content', 'none');
	wp_reset_postdata();
	endif;
	?>
</div>
</div>
</div>
</div>
<?php get_footer();