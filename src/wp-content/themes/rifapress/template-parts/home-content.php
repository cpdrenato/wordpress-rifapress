	<?php

	$imgDestaqueArray = get_field('galeria');

	$imageThumbURL = $imgDestaqueArray[0]['sizes'];
	
	$number_of_numbers = get_field('number_of_numbers');

	$inform_date = get_field('inform_date_the_draw');

	$date_of_draw = get_field('date_of_draw', false, false);
	
	$today = strtotime(date('Ymd'));		

	$checkDrawDate = strtotime($date_of_draw);
	
	$setStatuDate = (!empty($inform_date) && $today > $checkDrawDate ? 'disabled' : false);
	
	$raffle_title = get_the_title();
	
	$link = get_permalink();
	
	$thepost = $post->ID;
	
	$drawDate = get_field('date_of_draw');
	
	$currency = get_field('pro_currency', 'option');
	$thousands_separator = get_field('thousands_separator', 'option');
	$decimal_separator = get_field('decimal_separator', 'option');
	$number_decimal_places = get_field('number_decimal_places', 'option');
	$currency_position = get_field('currency_position', 'option');
	$valor = get_field('value_per_number');
    $getCurrency = !empty($currency) ? $currency : 'R$';
	$getThousandsSeparator = !empty($thousands_separator) ? $thousands_separator : '.';
	$getDecimalSeparator = !empty($decimal_separator) ? $decimal_separator : ',';
	$getNumberDecimal = !empty($number_decimal_places) ? $number_decimal_places : 2;
	
	$numbers = custom_get_post_selected_numbers($post->ID);

	$reserved = (!empty($numbers) ? sizeof($numbers) : 0);
	
	$available = intval($number_of_numbers) - $reserved;
	

	?>
	
<div class="main_card col-12 col-lg-4 mb-4">
<div class="card">
<a href="<?php echo $link; ?>">
<div class="position-relative">
	<?php
	if (!empty($valor)){
	?>
<?php if(!empty($currency_position) && $currency_position === 'left') : ?>
<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0; font-size: 2rem;"><?php _e($getCurrency , 'rifapress'); ?><?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></span>
<?php elseif(!empty($currency_position) && $currency_position === 'right') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0; font-size: 2rem;"> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?><?php _e($getCurrency , 'rifapress'); ?></span>
	<?php elseif(!empty($currency_position) && $currency_position === 'left_space') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"> <?php _e($getCurrency , 'rifapress'); ?> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></span>
	<?php elseif(!empty($currency_position) && $currency_position === 'right_space') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?> <?php _e($getCurrency , 'rifapress'); ?></span>
	<?php else: ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"> <?php _e($getCurrency , 'rifapress'); ?> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></span>
	<?php endif; ?>
	<?php }else{ ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"><?php _e('Valor', 'rifapress'); ?> <?php _e('sob consulta', 'rifapress'); ?></span>
	<?php } ?><?php if($setStatuDate == 'disabled') { echo'<span class="bg-danger position-absolute p-2 rounded shadow text-white text-uppercase bg_custom_statu" style="top:2px; right:4px;">Encerrado</span>'; }  ?>
	
	<?php  if(!empty($imgDestaqueArray)){ ?>

<img class="card-img-top"  src="<?php echo wp_custom_imgsort($thepost, 640, 360); ?>" alt=""<?php echo $raffle_title; ?>" title="<?php echo $raffle_title; ?>"/>

<?php }else{ ?>

<img class="card-img-top"  src="<?php echo wp_custom_no_img(640, 360); ?>" alt="<?php echo $raffle_title; ?>" title="<?php echo $raffle_title; ?>"/>

<?php } ?>

</div>
</a>
<div class="card-body text-center">
<a class="text-decoration-none text-dark color_main_title" href="<?php echo $link; ?>"><h5 class="card-title color_main_title"><?php echo $raffle_title; ?></h5></a>

<?php if (!empty($number_of_numbers)): ?>
    <span class="btn btn-light text-success btn-sm">Números <br><?php echo $number_of_numbers; ?></span>
	<?php endif; ?>	
	<span class="btn btn-light text-warning btn-sm">Reservados <br><?php echo $reserved; ?></span>
	<span class="btn btn-light text-danger btn-sm">Restantes <br><?php echo $available; ?></span>
</div>
<div class="text-center">
<?php if($setStatuDate == 'disabled') { ?> 
<a href="<?php echo $link; ?>" id="<?php echo $thepost; ?>" class="btn btn-danger d-block text-uppercase">Ver Resultado</a>
<?php } else { ?>
<a href="<?php echo $link; ?>" id="<?php echo $thepost; ?>" class="btn btn-success d-block text-uppercase buy_raffle">comprar rifa</a>

<?php } ?>
</div>
</div>
</div>