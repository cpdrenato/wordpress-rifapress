	<?php
	$args = array(
	'post_type' => 'post',
	'meta_key' => 'relevance',
	'meta_value' => '1',
	'posts_per_page' => 12,
	'orderby' => 'rand');
	$querySlide = new WP_Query($args);
	if ($querySlide->have_posts()): ?>
	<?php $lotteryThemeUri = get_template_directory_uri(); ?>
	
	
<div class="slide">
<div id="carouselIndicators" class="carousel slide carousel-fade" data-ride="carousel">
<div class="carousel-inner">
	
	
	<?php
	$count = 0;
	while ($querySlide->have_posts()): $querySlide->the_post();
	$imgSlide = get_field('galeria');
	$active = ($count == 0 ? 'active' : false); 
	$count++; 
	if (!empty($imgSlide)){ ?>
	<div class="carousel-item <?php echo $active; ?>">
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
<picture>
 <source srcset="<?php echo wp_custom_img(1920, 720); ?>" media="(min-width: 1400px)">
 <source srcset="<?php echo wp_custom_img(1080, 720); ?>" media="(min-width: 768px)">
 <source srcset="<?php echo wp_custom_img(800, 450); ?>" media="(min-width: 576px)">
<img src="<?php echo wp_custom_img(593, 334); ?>" class="d-block w-100 img-fluid" alt="<?php the_title(); ?>">
</picture>
</a>
<div class="carousel-caption color_custom_title_slide">
<h3 class="display-4 font-weight-bold"><?php title_limite(58); ?></h3>
<div class="text-center">
<a href="<?php the_permalink(); ?>" class="btn btn-success btn-lg text-uppercase p-3 buy_raffle">comprar rifa</a>
</div>
</div>
</div>
	<?php }else{ ?>
	<div class="carousel-item <?php echo $active; ?>">
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
<picture>
 <source srcset="<?php echo wp_custom_no_img(1920, 720); ?>" media="(min-width: 1400px)">
 <source srcset="<?php echo wp_custom_no_img(1080, 720); ?>" media="(min-width: 768px)">
 <source srcset="<?php echo wp_custom_no_img(800, 450); ?>" media="(min-width: 576px)">
<img src="<?php echo wp_custom_no_img(593, 334); ?>" class="d-block w-100 img-fluid" alt="<?php the_title(); ?>">
</picture>
</a>
<div class="carousel-caption color_custom_title_slide">
<h3 class="display-4 font-weight-bold"><?php title_limite(58); ?></h3>
<div class="text-center">
<a href="<?php the_permalink(); ?>" class="btn btn-success btn-lg text-uppercase p-3 buy_raffle">comprar rifa</a>
</div>
</div>
</div>
	<?php } ?>
	<?php endwhile; wp_reset_postdata();?>			
</div>
<a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Anterior</span>
</a>
<a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
<span class="carousel-control-next-icon rotate_180deg" aria-hidden="true"></span>
<span class="sr-only">Próximo</span>
</a>
</div>
</div>
	<?php endif;
