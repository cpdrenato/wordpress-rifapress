	<?php
	$imgDestaqueArray = get_field('galeria');
	$imageThumbURL = $imgDestaqueArray[0]['sizes'];
	?>
	<div class="card mb-4">
	<div class="row">
	<div class="col-md-6">
	<a href="<?php the_permalink(); ?>">
	<div class="position-relative">
	<?php
	$currency = get_field('pro_currency', 'option');
	$thousands_separator = get_field('thousands_separator', 'option');
	$decimal_separator = get_field('decimal_separator', 'option');
	$number_decimal_places = get_field('number_decimal_places', 'option');
	$currency_position = get_field('currency_position', 'option');
	$valor = get_field('value_per_number');
	$getCurrency = !empty($currency) ? $currency : 'R$';
	$getThousandsSeparator = !empty($thousands_separator) ? $thousands_separator : '.';
	$getDecimalSeparator = !empty($decimal_separator) ? $decimal_separator : ',';
	$getNumberDecimal = !empty($number_decimal_places) ? $number_decimal_places : 2;
	if (!empty($valor)){
	?>
	<?php if(!empty($currency_position) && $currency_position === 'left') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0; font-size: 2rem;"><?php _e($getCurrency , 'rifapress'); ?><?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></span>
	<?php elseif(!empty($currency_position) && $currency_position === 'right') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0; font-size: 2rem;"> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?><?php _e($getCurrency , 'rifapress'); ?></span>
	<?php elseif(!empty($currency_position) && $currency_position === 'left_space') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"> <?php _e($getCurrency , 'rifapress'); ?> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></span>
	<?php elseif(!empty($currency_position) && $currency_position === 'right_space') : ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?> <?php _e($getCurrency , 'rifapress'); ?></span>
	<?php else: ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"> <?php _e($getCurrency , 'rifapress'); ?> <?php echo number_format($valor, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></span>
	<?php endif; ?>
	<?php }else{ ?>
	<span class="bg-transparent p-1 position-absolute shadow text-white text-uppercase font-weight-bold bg_custom_price" style="bottom:0; left:0;font-size: 2rem;"><?php _e('Valor', 'rifapress'); ?> <?php _e('sob consulta', 'rifapress'); ?></span>
	<?php } ?>
	<?php 
	if(!empty($imgDestaqueArray)){ 
	?>
	<img class="card-img-top" src="<?php echo wp_custom_img(640, 360); ?>" alt="<?php the_title_attribute(); ?>" title="<?php the_title_attribute(); ?>" />
	<?php }else{ ?>
	<img class="card-img-top" src="<?php echo wp_custom_no_img(640, 360); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
	<?php } ?>
	</div>
	</a>
	</div>
	<div class="col-md-6 d-flex align-self-center">
	<div class="card-body text-center p-0">
	<a class="text-decoration-none text-dark color_main_title" href="<?php the_permalink(); ?>"><h5 class="card-title p-2 color_main_title"><?php the_title(); ?></h5></a>
	<?php
	$number_of_numbers = get_field('number_of_numbers');
	if (!empty($number_of_numbers)): ?>
	<span class="btn btn-light text-success btn-sm mb-2">Números <br><?php echo $number_of_numbers; ?></span>
	<?php endif; ?>	
	<?php 
	$numbers = custom_get_post_selected_numbers($post->ID);
	$reserved = (!empty($numbers) ? sizeof($numbers) : 0);
	?>	
	<span class="btn btn-light text-warning btn-sm mb-2">Reservados <br><?php echo $reserved; ?></span>
	<?php
	$available = intval($number_of_numbers) - $reserved; ?>
	<span class="btn btn-light text-danger btn-sm mb-2">Restantes <br><?php echo $available; ?></span>
	</div>
	</div>
	</div>
	</div>

