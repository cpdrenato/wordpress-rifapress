<style>.no_results{text-align:center;margin-bottom:10px}img.icon_no_results{width:100%;max-width:80px;min-height:120px}</style>
<div class="art_result">
<?php $themeURI = get_template_directory_uri(); ?>
    <div class="no_results">
	<img src="<?php echo $themeURI;?>/img/search.svg" title="Nenhum resultado" alt="Nenhum resultado" class="icon_no_results" />
	</div>
    <?php echo '<p class="text-center"> Nenhuma rifa encontrada com as características da sua pesquisa, tente outros termos.</p>'; ?>
</div>
