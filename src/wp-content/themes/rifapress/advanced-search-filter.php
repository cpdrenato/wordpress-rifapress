<form class="bg-light p-2" action="<?php echo esc_url(home_url('/')); ?>" id="advanced_form_filter">
        <input type="hidden"  name="rifa" value="pesquisa">
          <input type="hidden" name="s"/>
		        <?php if (!empty($_GET['order'])) : ?>
				<input type="hidden" name="order" value="<?php echo $_GET['order']; ?>">
				<?php endif;?>
<div class="form-group">
<?php $termId = isset(get_queried_object()->term_id) ? get_queried_object()->term_id : false ; $termCat = !empty($termId) ? get_term($termId, 'category') : false; ?>
<label for="tipo">Categoria</label>
<select class="custom-select custom-select-lg mb-3 jq_disabled" name="tipo">
	<option value="" selected="selected">Todas categorias</option>
	<?php
	$categories = get_categories();
	if ($categories):
	foreach ($categories as $category):
	?>
	<option value="<?php echo $category->slug; ?>" <?php if(isset($_GET['tipo']) && $_GET['tipo'] === $category->slug || !empty($termCat) && $termCat->slug === $category->slug) echo "selected"; ?>>
	<?php echo $category->cat_name; ?></option>
	<?php
	endforeach;
	endif;
	?> 
</select>
</div>
<div class="form-group">
<?php $terms = get_terms('states', array('hide_empty' => true,)); ?>
<label for="state">Estado</label>
<select class="custom-select custom-select-lg mb-3 jq_disabled" name="state">
<option value="" selected><?php _e('Todos estados '); ?></option>
<?php
if ($terms):
foreach ($terms as $term):
?>
<option value="<?php echo $term->slug;?>" <?php if(isset($_GET['state']) && $_GET['state'] === $term->slug) echo "selected"; ?>> <?php echo $term->name; ?> </option>
<?php
endforeach;
endif;
?>
</select>
</div>
<div class="form-group">
<label for="city">Cidade</label>
<?php if(empty($_GET['state'])) : ?>
<select class="custom-select custom-select-lg mb-3 jq_disabled" name="city" disabled>
<option value="" selected disabled><?php _e('Selecione um estado'); ?></option>
</select>
<?php elseif(!empty($_GET['state'])): get_city_filter(); endif; ?>
</div>
<?php
$currency = get_field('pro_currency', 'option');
$currency = (!empty($currency) ? $currency : 'R$'); ?>
  <div class="form-row">
    <div class="col">
	<label for="valorMin_id">Preço Min.</label>
<input type="tel" name="valor_min" placeholder="<?php echo $currency; ?>" class="form-control form-control-lg jq_disabled" id="valorMin_id" value="<?php if(!empty($_GET['valor_min']) && $_GET['valor_min'] != 'R$ 0,00') echo $_GET['valor_min']; ?>"></input>
    </div>
    <div class="col">
	<label for="valorMax_id">Preço Max.</label>
<input type="tel"  name="valor_max" placeholder="<?php echo $currency; ?> ilimitado" class="form-control form-control-lg jq_disabled" id="valorMax_id" value="<?php if(!empty($_GET['valor_max']) && $_GET['valor_max']!= 'R$ 0,00') echo $_GET['valor_max']; ?>">
</input>
    </div>
  </div>
</form>