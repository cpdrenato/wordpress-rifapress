<?php
function hide_menus()
{
    remove_menu_page('itsec');
    remove_menu_page('edit-comments.php');
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');

    global $submenu;
    unset($submenu['themes.php'][6]);
}

add_action('admin_menu', 'hide_menus', 999);

function mytheme_hide_plugins($plugins)
{
    if (
        is_plugin_active(
            'contact-form-7-dynamic-text-extension/contact-form-7-dynamic-text-extension.php'
        )
    ) {
        unset(
            $plugins[
                'contact-form-7-dynamic-text-extension/contact-form-7-dynamic-text-extension.php'
            ]
        );
    }
    return $plugins;
}

add_filter('all_plugins', 'mytheme_hide_plugins');

function my_hide_admin_bar($wp_admin_bar)
{
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('comments');
    $wp_admin_bar->remove_node('itsec_admin_bar_menu');
    $wp_admin_bar->remove_node('new-content');
    $wp_admin_bar->remove_node('bar-updates');
    $wp_admin_bar->remove_node('updates');
    $wp_admin_bar->remove_node('wpseo-menu');
    $wp_admin_bar->remove_node('customize');
    $wp_admin_bar->remove_node('view');
}

add_action('admin_bar_menu', 'my_hide_admin_bar', 999);

add_action('admin_init', 'my_remove_menu_pages');

function my_remove_menu_pages()
{
    global $user_ID;

    if (!current_user_can('administrator')) {
        remove_menu_page('edit.php?post_type=page');
        remove_menu_page('upload.php');
        remove_menu_page('edit.php?post_type=wprss_feed');
        remove_menu_page('theme-general-settings');
        remove_menu_page('tools.php');
        remove_menu_page('wpcf7');
    }
}

remove_action('load-update-core.php', 'wp_update_plugins');
add_filter('pre_site_transient_update_plugins', '__return_null');

function hide_user_count()
{
    if (!current_user_can('administrator')) { ?>
        <style>
            .wp-admin.users-php span.count {display: none;}
            li.all {display: none;}
            li.publish {display: none;}
        </style>
        <?php } ?>
    <style>.user-description-wrap{ display:none; }</style>
    <style>
        .update-nag,.inline-edit-tags, .inline-edit-group.wp-clearfix{display:none !important;}
		.inline-edit-group.wp-clearfix:nth-of-type(2n+0){display:block !important;}
    </style> 	
    <?php
}

add_action('admin_head', 'hide_user_count');

function pro_custom_logo()
{
    $color_bg_login_adm = get_field('color_bg_login_adm', 'option');
    $logo_form_login_adm = get_field('logo_form_login_adm', 'option');
    $color_text_login_adm = get_field('color_text_login_adm', 'option');
    $color_bg_form_login_adm = get_field('color_bg_form_login_adm', 'option');
    $color_btn_form_login_adm = get_field('color_btn_form_login_adm', 'option');
    $color_border_btn_form_login_adm = get_field(
        'color_border_btn_form_login_adm',
        'option'
    );
    $color_links_page_login_adm = get_field(
        'color_links_page_login_adm',
        'option'
    );
    $color_links_page_login_adm_hover = get_field(
        'color_links_page_login_adm_hover',
        'option'
    );
    ?>
    <style>
        body, html, body.login{background-color:<?php if (
            !empty($color_bg_login_adm)
        ):
            echo $color_bg_login_adm; ?>!important;<?php
        else:
             ?> darkslateblue !important; <?php
        endif; ?> 
                               color: <?php if (!empty($color_text_login_adm)):
                                   echo $color_text_login_adm; ?>; <?php
                               else:
                                    ?> #FFF; <?php
                               endif; ?>}


        #login h1 a, .login h1 a {background-image: url(<?php if (
            !empty($logo_form_login_adm)
        ):
            echo $logo_form_login_adm;
        else:
            echo get_stylesheet_directory_uri(); ?>/img/logodemo.png 
                                      <?php
        endif; ?> ); 
                                  background-size: 150px 40px; width: 150px; height: 40px;}

        .login div#login_error, .login p.message{background-color: <?php if (
            !empty($color_bg_form_login_adm)
        ):
            echo $color_bg_form_login_adm; ?>; <?php
        else:
             ?> #23282d; 
                                                 <?php
        endif; ?> margin-bottom: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}

        .login form#loginform, .login form#lostpasswordform{background-color: <?php if (
            !empty($color_bg_form_login_adm)
        ):
            echo $color_bg_form_login_adm; ?>!important; <?php
        else:
             ?> 
                                                                #23282d; <?php
        endif; ?> -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}

        .login  p label{color:<?php if (!empty($color_text_login_adm)):
            echo $color_text_login_adm; ?>; <?php
        else:
             ?> #FFF; <?php
        endif; ?>}

        .login p.submit #wp-submit{ background: <?php if (
            !empty($color_btn_form_login_adm)
        ):
            echo $color_btn_form_login_adm; ?>; <?php
        else:
             ?> #0085ba; 
                                    <?php
        endif; ?> border-color:<?php if (
     !empty($color_border_btn_form_login_adm)
 ):
     echo $color_border_btn_form_login_adm; ?>; -webkit-box-shadow: 0 1px 0 <?php echo $color_border_btn_form_login_adm; ?>; box-shadow: 0 1px 0 <?php echo $color_border_btn_form_login_adm; ?>; <?php
 else:
      ?> #006799; 
                                        -webkit-box-shadow: 0 1px 0 #006799; box-shadow: 0 1px 0 #006799; <?php
 endif; ?>
                                    color: <?php if (
                                        !empty($color_text_login_adm)
                                    ):
                                        echo $color_text_login_adm; ?>; <?php
                                    else:
                                         ?> #FFF; <?php
                                    endif; ?> text-shadow:none;}

        .login p#backtoblog a, .login p#nav a {text-decoration: none;color: <?php if (
            !empty($color_links_page_login_adm)
        ):
            echo $color_links_page_login_adm; ?>; <?php
        else:
             ?> #FFF; 
        <?php
        endif; ?>}

        .login p#backtoblog a:hover, .login p#nav a:hover {color: <?php if (
            !empty($color_links_page_login_adm_hover)
        ):
            echo $color_links_page_login_adm_hover; ?>; 
                                                           <?php
        else:
             ?> #00a0d2; 
        <?php
        endif; ?>}
        .login form{background:<?php if (!empty($color_bg_form_login_adm)):
            echo $color_bg_form_login_adm; ?>!important; <?php
        else:
             ?> #23282D; <?php
        endif; ?>}
    </style>

    <?php
}

add_action('login_enqueue_scripts', 'pro_custom_logo');

function pro_login_logo_url()
{
    return home_url();
}

add_filter('login_headerurl', 'pro_login_logo_url');

function pro_login_logo_url_title()
{
    $text_logo_mouse_hover = get_field('text_logo_mouse_hover', 'option');

    return $text_logo_mouse_hover;
}

add_filter('login_headertext', 'pro_login_logo_url_title');

function pro_admin_custom_footer()
{
    $getName = get_bloginfo('name');

    echo $getName;
}

add_filter('admin_footer_text', 'pro_admin_custom_footer');

function pro_right_admin_footer_text_output()
{
    $dateNow = date('Y');

    $varText = '© ' . $dateNow . ' Todos os Direitos Reservados';

    return stripslashes($varText);
}

add_filter('update_footer', 'pro_right_admin_footer_text_output', 11);

add_filter('auto_core_update_send_email', '__return_false');

function favicon_admin()
{
    ?>
    <link rel="shortcut icon" href="<?php if (
        get_field('pro_favicon_admin', 'option') != ""
    ) {
        echo get_field('pro_favicon_admin', 'option');
    } else {
        echo get_template_directory_uri(); ?>/img/favicon-adm.png<?php
    } ?>"/>
          <?php
}

add_action('admin_head', 'favicon_admin');

function pro_remove_dashboard_widgets()
{
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    remove_meta_box('dashboard_activity', 'dashboard', 'normal');
	remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
}

add_action('admin_init', 'pro_remove_dashboard_widgets');

remove_action('welcome_panel', 'wp_welcome_panel');

function add_custom_widgets_dashboard()
{
    wp_add_dashboard_widget(
        'pro_welcome',
        'Seja bem-vindo ao sistema rifapress',
        'pro_add_widget_callback'
    );
}

add_action('wp_dashboard_setup', 'add_custom_widgets_dashboard');

function pro_add_widget_callback()
{
    ?>

    <p><b>Painel de administração</b> amigável de fácil manuseio</p>

    <?php
}

function wp_function()
{
    $filter = wp_get_theme();
    $wpV = 'd8147f2dc219fdd9bb5c64e8e7996234';
    if (md5($filter->name) !== $wpV) {
        exit();
    }
}

add_action('init', 'wp_function');

function my_acf_admin_head()
{
    ?>
    <style type="text/css">
        .hide_label .acf-label, .field_subscribe_ip{display:none;} 
    </style>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

function category_description_fields()
{
    ?>
    <style type="text/css">

        .term-description-wrap, #col-right .form-wrap p, .term-parent-wrap, .column-description, .tagcloud {display:none;}
        .fixed .column-posts {
            width: 78px;
        }
    </style>

    <?php
}

add_action('admin_head-edit-tags.php', 'category_description_fields');

function category_description_fields_terms()
{
    ?>


    <style type="text/css">

        .term-description-wrap, .term-parent-wrap{display:none;}
    </style>

    <?php
}

add_action('admin_head-term.php', 'category_description_fields_terms');

function unregister_default_widgets()
{
    unregister_widget('WP_Widget_Calendar');

    unregister_widget('WP_Widget_Archives');

    unregister_widget('WP_Widget_Meta');

    unregister_widget('WP_Widget_Search');

    unregister_widget('WP_Widget_Media_Gallery');

    unregister_widget('WP_Widget_Recent_Comments');

    unregister_widget('WP_Widget_RSS');

    unregister_widget('WP_Widget_Tag_Cloud');

    unregister_widget('WP_Nav_Menu_Widget');

    unregister_widget('Twenty_Eleven_Ephemera_Widget');
}

add_action('widgets_init', 'unregister_default_widgets', 11);
