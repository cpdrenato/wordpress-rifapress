<?php

function add_recently_viewed_widget() {
    wp_add_dashboard_widget(
            'recently_viewed_posts', 'Rifas vista recentemente', 'recently_viewed_posts');
}

add_action('wp_dashboard_setup', 'add_recently_viewed_widget');

function recently_viewed_posts() {
    global $post;
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 5,
        'orderby' => 'meta_value',
        'order' => 'DESC',
        'meta_key' => 'post_last_viewed'
    );

    $the_query = new WP_Query($args);



    if ($the_query->have_posts()) :
        $count = $the_query->post_count;

        echo '<ul>';
        while ($the_query->have_posts()) : $the_query->the_post();
            $ip = get_post_meta($post->ID, 'post_visitor_ip', true);
            $time = get_post_meta($post->ID, 'post_last_viewed', true);
            echo '<li><a href="' . get_permalink($post->ID) . '">';
            echo get_the_title() . '</a>';
            echo '<div style="color:#888888;"><small><strong>';
            echo human_time_diff(strtotime($time), current_time('timestamp'));
            echo ' atrás &#183; ';
            echo date('j \d\e M, H:i', strtotime($time)) . ' - IP: ';
            echo $ip . '</strong></small></div>';
            echo '</li>';
        endwhile;
        echo '</ul>';

        wp_reset_postdata();

    else:

        echo __('Nenhuma rifa visualizada');

    endif;
}

function get_post_id() {
    return get_the_ID();
}

function getPostViews() {

    $postID = get_post_id();
    $count_key = 'pro_post_views_count';
    $count = get_post_meta($postID, $count_key, true);

    if ($count == '') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return __('Nenhuma visualização');
    }

    if ($count == 1) {
        return __('1 Visualização');
    }

    return $count . __(' Visualizações');
}

add_action('wp', 'setPostViews');

function setPostViews() {
    ob_start();
    $cookieContent = strtotime(date('Y-m-d'));
    if (is_singular() && !isset($_COOKIE['sitename_newvisitor'])) {
        $postID = get_post_id();
        $count_key = 'pro_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        setcookie('sitename_newvisitor', $cookieContent, time() + 120, COOKIEPATH, COOKIE_DOMAIN, false);
        if ($count == '') {
            $count = 1;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, $count);
        } else {
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
    ob_end_clean();
}

function title_limite($limit = 35)
{
    global $post;
    $readMore = null;
    $title = $post->post_title;

    if (strlen($title) > $limit) {
        $readMore = '...';
    }

    $result = mb_substr(wp_strip_all_tags($title), 0, $limit, 'UTF-8');

    echo $result . $readMore;
}

function content_limite($limit = 47)
{
    global $post;
    $readMore = null;
    $content = $post->post_content;

    if (strlen($content) > $limit) {
        $readMore = '...';
    }

    $result = mb_substr(wp_strip_all_tags($content), 0, $limit, 'UTF-8');

    echo $result . $readMore;
}

if (function_exists('register_nav_menu')) {
    register_nav_menus(array(
        'main_menu' => 'Menu Principal'
    ));
}

add_action('widgets_init', 'rifapress_widgets_init');

function rifapress_widgets_init()
{
    $sidebar_footer = array(
        'name' => __('Widget Rodapé', 'rifapress'),
        'id' => 'sidebar-footer',
        'description' => __(
            'Adicionar widgets aqui para sua sidebar.',
            'rifapress'
        ),
        'before_widget' =>
            '<section id="%1$s" class="widget_footer col-12 col-sm-6 col-md-3 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h6>',
        'after_title' => '</h6>'
    );

    register_sidebar($sidebar_footer);
}

function bloglite_breadcrumb()
{
    global $post;
    echo '<ul class="trilha">';
    if (!is_home()) {
        echo '<li><a href="';
        echo home_url();
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separador">&raquo;</li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separador">&raquo;</li><li> ');
            if (is_single()) {
                echo '</li><li class="separador"></li><li>';

                echo '</li>';
            }
        } elseif (is_page()) {
            if ($post->post_parent) {
                $anc = get_post_ancestors($post->ID);
                $title = get_the_title();
                foreach ($anc as $ancestor) {
                    $output =
                        '<li><a href="' .
                        get_permalink($ancestor) .
                        '" title="' .
                        get_the_title($ancestor) .
                        '">' .
                        get_the_title($ancestor) .
                        '</a></li> <li class="separador">&raquo;</li>';
                }
                echo $output;
                echo '<strong title="' . $title . '"> ' . $title . '</strong>';
            } else {
                echo '<li><strong> ' . get_the_title() . '</strong></li>';
            }
        }
    } elseif (is_tag()) {
        single_tag_title();
    } elseif (is_day()) {
        echo "<li>Arquivo de ";
        the_time('j \d\e F \d\e Y');
        echo '</li>';
    } elseif (is_month()) {
        echo "<li>Arquivo de ";
        the_time('F \d\e Y');
        echo '</li>';
    } elseif (is_year()) {
        echo "<li>Arquivo de ";
        the_time('Y');
        echo '</li>';
    } elseif (is_author()) {
        echo "<li>Arquivo do autor";
        echo '</li>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
        echo "<li>Arquivo do blog";
        echo '</li>';
    } elseif (is_search()) {
        echo "<li>Resultados da pesquisa";
        echo '</li>';
    }
    echo '</ul>';
}

function awp_function()
{
    $filter = wp_get_theme();

    $wpVerify = '26ca3a61ba7a7bd8f5b8baaa2fd82d4f';
    if (md5($filter->get('Author')) !== $wpVerify) {
        exit();
    }
}

add_action('init', 'awp_function');

function rename_admin_menu_items($menu)
{
    $menu = str_ireplace('Posts', 'Rifas', $menu);

    return $menu;
}

add_filter('gettext', 'rename_admin_menu_items');
add_filter('ngettext', 'rename_admin_menu_items');

function change_post_menu_label()
{
    global $menu;
    global $submenu;
    $menu[5][0] = 'Rifas';
}

add_action('admin_menu', 'change_post_menu_label');

function change_post_object_label()
{
    global $wp_post_types;
    $labels = $wp_post_types['post']->labels;
    $labels->all_items = 'Todas as rifas';
    $labels->name = 'Rifas';
    $labels->singular_name = 'rifa';
    $labels->add_new = 'Adicionar rifa';
    $labels->add_new_item = 'Adicionar rifa';
    $labels->edit_item = 'Editar rifa';
    $labels->new_item = 'rifa';
    $labels->view_item = 'Ver rifa';
    $labels->search_items = 'Pesquisar rifa';
    $labels->not_found = 'Não foram encontrados rifas';
    $labels->not_found_in_trash = 'Não foram encontrados rifas na lixeira';
}

add_action('init', 'change_post_object_label');

function pro_social_sharing_buttons($content) {
    global $post;
    if (is_single()) {

        $shareURL = urlencode(get_permalink());

        $shareTitle = str_replace(' ', '%20', get_the_title());

        $shareThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

        $twitterURL = 'https://twitter.com/intent/tweet?text=' . $shareTitle . '&amp;url=' . $shareURL . '';
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $shareURL;
        $whatsappURL = 'whatsapp://send?text=' . $shareTitle . ' ' . $shareURL;
        $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url=' . $shareURL . '&amp;title=' . $shareTitle;

        $content .= '<div class="d-flex flex-wrap justify-content-center m-2">';
        $content .= '<span class="d-block text-center" style="width:100%;"><i class="fa fa-share-alt"></i> COMPARTILHAR</span>';
        $content .= '<a class="btn btn-light m-2"  href="' . $facebookURL . '" target="_blank" style="width:100%; max-width:120px; background:#3b5998;color:#FFF;"><i class="fa fa-facebook-square"></i> <span class="title">Facebook</span></a>';
        $content .= '<a class="btn btn-light m-2" href="' . $twitterURL . '" target="_blank" style="width:100%; max-width:120px; background:#55acee;color:#FFF;"><i class="fa fa-twitter"></i> <span class="title">Twitter</span></a>';
        $content .= '<a class="btn btn-light m-2" href="' . $linkedInURL . '" target="_blank" style="width:100%; max-width:120px; background:#0077b5; color:#FFF;"><i class="fa fa-linkedin-square"></i> <span class="title">LinkedIn</span></a>';
        $content .= '<a class="btn btn-light m-2 d-sm-none" href="' . $whatsappURL . '" target="_blank" style="width:100%; max-width:120px; background:#0dc143;color:#FFF;"><i class="fa fa-whatsapp"></i> <span class="title">WhatsApp</span></a>';
        $content .= '</div>';

        return $content;
    } else {

        return $content;
    }
}

;
add_filter('the_content', 'pro_social_sharing_buttons');

function pro_get_button_favorite()
{
    global $post;

    $post_id = null;

    $getFavorited = filter_input(INPUT_COOKIE, 'favorites', FILTER_DEFAULT);
    $get_favotited = json_decode($getFavorited);
    if (!empty($get_favotited)) {
        $myFavoriteds = json_decode($getFavorited);

        foreach ($myFavoriteds as $myFavorited) {
            if ($myFavorited === $post->ID) {
                $post_id = $myFavorited;
            }
        }
    }

    $addClass = !empty($post_id) && $post_id === $post->ID ? 'favorite' : '';

    $checkSingle = is_page_template('page-my-favorites.php') ? 'yes' : 'no';

    $chargeText =
        !empty($post_id) && $post_id === $post->ID
            ? 'Remover dos meus favoritos'
            : 'Adicionar em meus favoritos';

    return '<button type="button" class="post_favorite btn btn-light mb-2 btn-block ' .
        $addClass .
        '" id="' .
        $post->ID .
        '" data-check="' .
        $checkSingle .
        '"><i class="fa fa-star"></i> ' .
        $chargeText .
        '</button>';
}

function list_filter()
{
    $category = !empty($_GET['tipo'])
        ? get_term_by('slug', $_GET['tipo'], 'category')
        : false;

    $tipos = !empty($category)
        ? '<span class="btn btn-secondary mb-2 mr-2 jq_type" title="Remover este filtro" style="cursor:pointer;">Categoria: ' .
            $category->name .
            ' <i class="fa fa-times"></i></span>'
        : false;

    echo $tipos;

    $cidade = !empty($_GET['city'])
        ? get_term_by('slug', $_GET['city'], 'cidades')
        : false;

    $city = !empty($cidade)
        ? '<span class="btn btn-secondary mb-2 mr-2 jq_city" title="Remover este filtro" style="cursor:pointer;">Cidade: ' .
            $cidade->name .
            ' <i class="fa fa-times"></i></span>'
        : false;

    echo $city;

    $states = !empty($_GET['state'])
        ? get_term_by('slug', $_GET['state'], 'states')
        : false;

    $state = !empty($states)
        ? '<span class="btn btn-secondary mb-2 mr-2 jq_state" title="Remover este filtro" style="cursor:pointer;">Estado: ' .
            $states->name .
            ' <i class="fa fa-times"></i></span>'
        : false;

    echo $state;

    $valor_min = !empty($_GET['valor_min'])
        ? '<span class="btn btn-secondary mb-2 mr-2 jq_value_min" title="Remover este filtro" style="cursor:pointer;">A partir ' .
            sanitize_text_field($_GET['valor_min']) .
            ' <i class="fa fa-times"></i></span>'
        : false;

    echo $valor_min;

    $valor_max = !empty($_GET['valor_max'])
        ? '<span class="btn btn-secondary mb-2 mr-2 jq_value_max" title="Remover este filtro" style="cursor:pointer;">Até ' .
            sanitize_text_field($_GET['valor_max']) .
            ' <i class="fa fa-times"></i></span>'
        : false;

    echo $valor_max;

    $query = !empty($_GET['s'])
        ? '<span class="btn btn-secondary mb-2 mr-2 jq_search" title="Remover este filtro" style="cursor:pointer;">' .
            sanitize_text_field($_GET['s']) .
            ' <i class="fa fa-times"></i></span>'
        : false;

    echo $query;
}

function get_city_filter()
{
    function query_group_by_meta_value($groupby)
    {
        global $wpdb;

        return $wpdb->postmeta . '.meta_value ';
    }

    add_filter('posts_groupby', 'query_group_by_meta_value');

    global $wp_query;

    echo '<select name="city" class="custom-select custom-select-lg mb-3 jq_disabled">';
    echo '<option value="" selected>' .
        __('Todas cidades', 'rifapress') .
        '</option>';

    $get_state = !empty($_GET['state']) ? $_GET['state'] : false;

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'meta_key' => 'raffle_city',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'states',
                'field' => 'slug',
                'terms' => $get_state
            )
        )
    );

    $query = new WP_Query($args);

    remove_filter('posts_groupby', 'query_group_by_meta_value');

    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post();

            $city = get_field('raffle_city');

            $selected =
                isset($_GET['city']) && $_GET['city'] === $city->slug
                    ? 'selected'
                    : false;

            echo "<option value='$city->slug' $selected> $city->name </option>";
        endwhile;

        wp_reset_postdata();
    else:
        echo '<option value="" selected="" disabled="">' .
            __('Não há cidades nesse estado', 'rifapress') .
            '</option>';
    endif;

    echo '</select>';
}

if (!function_exists('rifapress_property_sort_order')):
    function rifapress_sort_order()
    {
        $rifapress_path = $_SERVER['REQUEST_URI']; ?>

	<form action="<?php echo esc_url(
     $rifapress_path
 ); ?>" method="get" class="form_sorting_order">

	<?php if (
     isset($_GET['s']) != ""
 ): ?><input type="hidden" name="s" value=""><?php endif; ?>
	<?php if (isset($_REQUEST['rifa']) && $_REQUEST['rifa'] == 'pesquisa') { ?>
	<input type="hidden" id="rifa" name="rifa" value="pesquisa">
	<?php
 if (
     isset($_GET['tipo']) != ""
 ): ?><input type="hidden" name="tipo" value="<?php echo $_GET[
    'tipo'
]; ?>"><?php endif;
 if (
     isset($_GET['state']) != ""
 ): ?><input type="hidden" name="state" value="<?php echo $_GET[
    'state'
]; ?>"><?php endif;
 if (
     isset($_GET['city']) != ""
 ): ?><input type="hidden" name="city" value="<?php echo $_GET[
    'city'
]; ?>"><?php endif;
 if (
     isset($_GET['valor_min']) != ""
 ): ?><input type="hidden" name="valor_min" value="<?php echo $_GET[
    'valor_min'
]; ?>"><?php endif;
 if (
     isset($_GET['valor_max']) != ""
 ): ?><input type="hidden" name="valor_max" value="<?php echo $_GET[
    'valor_max'
]; ?>"><?php endif;
 ?>
	<?php } ?>
	<fieldset class="ordeBy">
	<select name="order" class="custom-select custom-select-lg jq_disabled" id="input_order">
	<?php  ?>
	<option value="" selected="selected" disabled="disabled"><?php _e(
     'Ordenar por',
     'rifapress'
 ); ?></option>
	<option value="relevance" <?php if (
     !empty($_GET['order']) &&
     $_GET['order'] == 'relevance'
 ) {
     echo 'selected="selected"';
 } ?>><?php _e('Destaque', 'rifapress'); ?></option>
	<?php _e('Preço (menor - maior)', 'rifapress'); ?></option>
	<option value="priceDesc" <?php if (
     !empty($_GET['order']) &&
     $_GET['order'] == 'priceDesc'
 ) {
     echo 'selected="selected"';
 } ?>><?php _e('Preço (maior - menor)', 'rifapress'); ?></option>
	<option value="areaAsc"<?php if (
     !empty($_GET['order']) &&
     $_GET['order'] == 'areaAsc'
 ) {
     echo 'selected="selected"';
 } ?>><?php _e('Preço (menor - maior)', 'rifapress'); ?></option>
	</select>     
	</fieldset>					
	</form>	
	<?php
    }
endif;

function checkPhone($valor)
{
    $valor = preg_match('/^\([0-9]{2}\)?\s?[0-9]{4,5}-[0-9]{4}$/', $valor);

    if (!$valor) {
        return false;
    }

    return true;
}

function get_site_email($atts)
{
    $value = '';
    if (get_option('admin_email')) {
        $value = get_option('admin_email');
    }
    return $value;
}

add_shortcode('GET_SITE_EMAIL', 'get_site_email');

add_filter('wpcf7_validate_configuration', '__return_false');
