<?php
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
	
		'page_title' 	=> 'rifapress opções v1.0',
		'menu_title'	=> 'Opções do tema',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'icon_url'     => 'dashicons-admin-generic',
		'redirect'		=> false,
	    'update_button'	=> __('Salvar alterações', 'acf'),
		'updated_message' => __("Alterações atualizadas", 'acf'),
		'position'		=> 9
	));
    acf_add_options_sub_page(array(
        'page_title' => __('Opções de moeda', 'rifapress'),
        'menu_title' => __('Configurar moeda', 'rifapress'),
        'parent_slug' => 'theme-general-settings',
	    'update_button'	=> __('Salvar alterações', 'acf'),
		'updated_message' => __("Alterações atualizadas", 'acf'),
    ));
    acf_add_options_sub_page(array(
        'page_title' => __('Opções de Contato', 'rifapress'),
        'menu_title' => __('Contato', 'rifapress'),
        'parent_slug' => 'theme-general-settings',
	    'update_button'	=> __('Salvar alterações', 'acf'),
		'updated_message' => __("Alterações atualizadas", 'acf'),
    ));
}
