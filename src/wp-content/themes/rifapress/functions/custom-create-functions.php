<?php

	function rifapress_create_new_post($title, $author, $numbers, $amount, $client_name, $client_phone, $post_object_id, $days){
				
	$new_post = array(

	 'post_title' => $title,
	 'post_status' => 'publish',
	 'post_author' => $author,
	 'post_type' => 'post_product_order',
	 'comment_status' => 'closed',
		 'meta_input'   => array(
			'status_order' => 'pending',
			'winner_order' => 'awaiting_draw',
			'client_order_numbers' => $numbers,
			'purchase_amount' => $amount,
			'client_name' => $client_name,
			'client_phone' => $client_phone,
			'post_object_id' => $post_object_id,
			'limit_order_remove' => $days
		),
	 );
	 
		 $post_id = wp_insert_post($new_post);
		 
				if( $post_id){
				 $my_post = array(
					'ID'           => $post_id,
					'post_title' => '#' . $post_id . ' '. $title,
				);
				
				wp_update_post( $my_post ); 
				}
		return $post_id;
		}
 
  function check_validate_purchase(){
	  
	    parse_str($_POST['data'], $data);
			
		if( !isset( $data['field_create_post'] ) || !wp_verify_nonce( $data['field_create_post'], 'action_create_post' ) ) {
			
		$response = [
		 'verify_nonce' => false,
		 'url' => site_url()
		];	
		
		wp_send_json_error( $response );

		exit;
			
		}else{
			
		$postId = intval($data['pid']);
						
		$userId = intval($data['uid']);
		
		$title = wp_strip_all_tags( $data['ptitle'] );
		
		$fullName = sanitize_text_field($data['name']);
		
		$phone = sanitize_text_field($data['phone']);
		
		$vdays = sanitize_text_field($data['vdays']);
		
		$vdays = intval($vdays);
		
		$termUse = sanitize_text_field($data['termUse']);
			
		$arrValues = custom_get_post_selected_numbers($postId);
		
		$numbers = $data['selectednumbers'];
		
		$numbersArr = explode(',', $numbers);
			
		$result = isInArray($numbersArr, $arrValues);
				
		if($result){
		$resultNumber = implode(',', $result);
		$msg = ( sizeof($result) > 1 ? 'Os números <b>' .$resultNumber. '</b> acabaram de ser reservados por outra pessoa. Por favor, escolha outros números' : 'O número <b>' .$resultNumber. '</b> acabou de ser reservado por outra pessoa. Por favor, escolha outro número' );
		$response = [
		 'action' => true,
		 'msg' => $msg,
		];
		
		wp_send_json_error( $response );			
			
		}
						
       	if(empty($fullName)){
			
		$response = [
		 'msg' => 'Por favor, preencha o nome completo.',
		 'field' => 'name',
		 'class' => 'error',
		];
		wp_send_json_error( $response );
		}
				
		if(strlen($fullName) < 10 ){
			
		$response = [
		 'msg' => 'O nome deve ter pelo menos 10 caracteres.',
		 'field' => 'name',
		 'class' => 'error',
		];
		
		wp_send_json_error( $response );
		
		}
			
       if(empty($phone)){
		   
		$response = [
		 'msg' => 'Por favor, digite telefone celular.',
		 'field' => 'phone',
		 'class' => 'error',
		];
		
		wp_send_json_error( $response );
		
		}	
				
       if(empty($termUse)){
		   
		$response = [
		 'msg' => 'Por favor, aceite os termos de uso.',
		 'field' => 'termUse',
		 'class' => 'error',
		];
		
		wp_send_json_error( $response );
		
		}	
										
	    $order_code = rifapress_create_new_post($title, $userId, $data['selectednumbers'], $data['vtotal'], $fullName, $phone, $postId, $vdays);


		$paymentdata = array("pid"=>$postId,"title"=>$title,"qtd"=>$data['qtd'],"vlitem"=>$data['vlitem'],"orderid"=>$order_code,"itens"=>$data['selectednumbers']);
		$paymentPacked = serialize($paymentdata);
		setcookie("purchase_data", $paymentPacked, false,"/",false);

					
		$response = [
		 'url' => site_url($data['_wp_http_referer']),
		 'eredirection' => true,
		 'redirectUrl' => site_url().'/opcoes-de-pagamento',
		];	
		
		wp_send_json_success( $response );
		
	}
	 exit();
	 
  }
  
add_action('wp_ajax_check_validate_purchase', 'check_validate_purchase');
add_action('wp_ajax_nopriv_check_validate_purchase', 'check_validate_purchase'); 