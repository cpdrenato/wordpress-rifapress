<?php

   function setPrice($price = null) {
	
	$currency = get_field('pro_currency', 'option');
	$thousands_separator = get_field('thousands_separator', 'option');
	$decimal_separator = get_field('decimal_separator', 'option');
	$currency = (!empty($currency) ? $currency : 'R$ ');
	$decimalSeparator = (!empty($decimal_separator) ? $decimal_separator : ',');
	$thousandsSeparator = (!empty($thousands_separator) ? $thousands_separator : '.');

    if (!empty($price)) {

        $price = substr($price, 0, strpos($price, $decimalSeparator));
        $price = str_replace($thousandsSeparator, '', $price);
        $price = str_replace($currency, '', $price);
    }

    return $price;
  }

  add_action('pre_get_posts', 'advanced_search_query');

   function advanced_search_query($query) {
	   
    if (isset($_REQUEST['rifa']) && $_REQUEST['rifa'] == 'pesquisa' && !is_admin() && $query->is_search && $query->is_main_query()) {
        $query->set('post_type', 'post');
        $tipo = isset($_REQUEST['tipo']) ? $_REQUEST['tipo'] : false;
        $vlMin = sanitize_text_field(isset($_REQUEST['valor_min'])) ? sanitize_text_field(setPrice($_REQUEST['valor_min'])) : (sanitize_text_field(isset($_REQUEST['valor_min'])) == 'R$ 0,00' ? false : false);
        $vlMax = sanitize_text_field(isset($_REQUEST['valor_max'])) ? sanitize_text_field(setPrice($_REQUEST['valor_max'])) : (sanitize_text_field(isset($_REQUEST['valor_max'])) == 'R$ 0,00' ? false : false);
        $state = isset($_REQUEST['state']) ? $_REQUEST['state'] : false;
        $city = isset($_REQUEST['city']) ? $_REQUEST['city'] : false;
        $filter = isset($_REQUEST['order']) ? $_REQUEST['order'] : false;
        $order = $filter === 'priceDesc' ? 'DESC' : 'ASC';
        $filter = ($filter === 'priceAsc' || $filter === 'priceDesc' ? 'value_per_number' : ($filter === 'relevance' ? 'relevance' : 'value_per_number'));
        $args = array();

        if (!empty($tipo)) :
            $args['tax_query'][] = array(
                'taxonomy' => 'category', 'field' => 'slug', 'terms' => $tipo
            );
        endif;


        if (!empty($vlMin) AND ( !empty($vlMax))):
            $args['meta_query'][] = array(
                'key' => 'value_per_number',
                'value' => array($vlMin, $vlMax),
                'type' => 'NUMERIC',
                'compare' => 'BETWEEN'
            );
        elseif (!empty($vlMin)):
            $args['meta_query'][] = array(
                'key' => 'value_per_number',
                'value' => $vlMin,
                'type' => 'NUMERIC',
                'compare' => '>='
            );
        elseif (!empty($vlMax)):
            $args['meta_query'][] = array(
                'key' => 'value_per_number',
                'value' => $vlMax,
                'type' => 'NUMERIC',
                'compare' => '<='
            );
        endif; 

        if (!empty($state)):
            $args['tax_query'][] = array(
                'taxonomy' => 'states',
                'field' => 'slug',
                'terms' => $state,
                'compare' => 'LIKE',
            );
        endif;
        if (!empty($city)):
            $args['tax_query'][] = array(
                'taxonomy' => 'cidades',
                'field' => 'slug',
                'terms' => $city,
                'compare' => 'LIKE',
            );
        endif;
        $query->set('tax_query', $args);
        $query->set('meta_query', $args);
        $query->set('meta_key', $filter);
        $query->set('orderby', 'meta_value_num');
        $query->set('order', $order);
    }
}

add_action('pre_get_posts', 'design_cat_posts_per_page');

function design_cat_posts_per_page($query) {

    if ($query->is_main_query() && is_category() && !is_admin()) {

        $filter = isset($_REQUEST['order']) ? $_REQUEST['order'] : false;
        $order = $filter === 'areaDesc' || $filter === 'priceDesc' ? 'DESC' : 'ASC';
        $filter = ($filter === 'priceAsc' || $filter === 'priceDesc' ? 'value_per_number' : ($filter === 'relevance' ? 'relevance' : 'value_per_number'));

        $query->set('meta_key', $filter);
        $query->set('orderby', 'meta_value_num');
        $query->set('order', $order);
    }
}


add_filter('pre_get_posts', 'page_search_exclude');

function page_search_exclude($query) {
    if ($query->is_main_query() && !is_admin()) {
        if ($query->is_search) {
            $filter = isset($_REQUEST['order']) ? $_REQUEST['order'] : false;
            $order = $filter === 'areaDesc' || $filter === 'priceDesc' ? 'DESC' : 'ASC';
            $filter = ($filter === 'priceAsc' || $filter === 'priceDesc' ? 'value_per_number' : ($filter === 'relevance' ? 'relevance' : 'value_per_number'));

            $query->set('post_type', 'post');
            $query->set('meta_key', $filter);
            $query->set('orderby', 'meta_value_num');
            $query->set('order', $order);
        }
    }
    return $query;
}
