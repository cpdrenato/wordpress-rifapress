<?php

add_filter('manage_post_posts_columns', 'set_custom_edit_property_columns', 10);

function set_custom_edit_property_columns($columns) {
    unset($columns['comments']);
    unset($columns['tags']);
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Título',
        'image' => 'Imagem',
        'categories' => 'Categorias',
        'cidades' => 'Cidade',
        'author' => 'Autor(a)',
        'date' => 'Data',
    );

    return $columns;
}
add_action('manage_post_posts_custom_column', 'custom_property_column', 10, 2);
function custom_property_column($column, $post_id) {
    ?>
    <style>
        .img_column{ border:none; width:100%; max-width:170px; height:auto;}
    </style>

    <?php
    $themaURL_img = get_template_directory_uri();
    $imgAdm = array('width' => 170, 'height' => 113, 'crop' => true);
    $urlImgAdm = bfi_thumb("$themaURL_img/uploads/no-image.jpg", $imgAdm);
    switch ($column) {

        case 'image' :
            $getImg = get_field('galeria', $post_id);
            if ($getImg)
                echo '<img class="img_column" src="' . wp_custom_img(170, 113) . '" />';
            else
               echo '<img class="img_column" src="' . wp_custom_no_img(170, 113) . '" />';

            break;

        case 'cidades' :
            $terms = get_the_term_list($post_id, 'cidades', '', ',', '');
            $terms = strip_tags($terms);
            if (is_string($terms))
                echo $terms;
            else
                _e('Nenhuma cidade', 'rifapress');
            break;

    }
}


function pro_column_register_sortable($columns) {
    $columns['cidades'] = 'Cidade';
    $columns['categories'] = 'Categorias';
    return $columns;
}

add_filter("manage_edit-post_sortable_columns", "pro_column_register_sortable");

function pro_custom_pages_columns($columns) {

    unset(
            $columns['comments']
    );

    return $columns;
}

add_filter('manage_pages_columns', 'pro_custom_pages_columns');

add_filter('manage_post_product_order_posts_columns', 'set_custom_edit_product_order_columns', 10);
add_action('manage_post_product_order_posts_custom_column', 'custom_product_order_column', 10, 2);

function set_custom_edit_product_order_columns($columns) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Pedido',
		'client_name' => 'Nome',
		'client_phone' => 'Telefone',
        'status_order' => 'Status',
        'purchase_amount' => 'Total', 
		'date' => 'Data',		
    );

    return $columns;
}

function custom_product_order_column($column, $post_id) {

       switch ($column) {
        case 'client_name' :

            $client_name = get_post_meta($post_id, 'client_name', true);
			
            if ($client_name)
				
                echo $client_name;
				
            else
                _e('Não há nome do cliente', 'rifapress');
			
            break;
			
			
        case 'client_phone' :

            $client_phone = get_post_meta($post_id, 'client_phone', true);
			
            if ($client_phone)
				
                echo $client_phone;
				
            else
                _e('Não há telefone do cliente', 'rifapress');
			
            break;
			
        case 'status_order' :

            $status_order = get_post_meta($post_id, 'status_order', true);
			$status_class = ($status_order == 'pending' ? 'pending' : ($status_order == 'paid' ? 'paid' : ( $status_order == 'completed' ? 'completed' : ( $status_order == 'canceled' ? 'canceled' : false))));
			$status_order = ($status_order == 'pending' ? 'Pendente' : ($status_order == 'paid' ? 'Pago' : ( $status_order == 'completed' ? 'Concluído' : ( $status_order == 'canceled' ? 'Cancelado' : false))));
            if ($status_order)
                echo '<span class="'.$status_class.'">'.$status_order.'</span>';
            else
                _e('Nenhum status', 'rifapress');
            break;

        case 'purchase_amount' :

            $purchase_amount = get_post_meta($post_id, 'purchase_amount', true);
			$purchase_amount = number_format($purchase_amount, 2, ',', '.');
            if ($purchase_amount)
                echo 'R$ ' .$purchase_amount;
            else
                _e('Nenhum valor', 'rifapress');
            break;
    }
}
