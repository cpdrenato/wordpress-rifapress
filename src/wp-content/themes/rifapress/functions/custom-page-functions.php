<?php
add_filter('gettext', 'change_admin_cpt_text_filter', 20, 3);

function change_admin_cpt_text_filter($translated_text, $untranslated_text, $domain) {

    global $typenow;

    if (is_admin() && 'post_product_order' == $typenow) {

        switch ($untranslated_text) {

            case 'Publish':
                $translated_text = __('Atualizar', 'rifapress');
                break;

            case 'Published':
                $translated_text = __('Efetuado', 'rifapress');
                break;
        }
    }
    return $translated_text;
}

function custom_get_post_selected_numbers($id) {
    $numbers = [];
    $args = array(
        'post_type' => 'post_product_order',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key' => 'post_object_id',
        'meta_value' => $id
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();

            $get_numbers = get_field('client_order_numbers');

            $get_numbers = explode(',', $get_numbers);

            $numbers[] = $get_numbers;
        } wp_reset_postdata();

        $numbers = array_merge([], ...$numbers);

        $numbers = array_unique($numbers);
    }

    return $numbers;
}

function custom_get_selected_numbers_status($id) {
    $numbers = [];
    $args = array(
        'post_type' => 'post_product_order',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key' => 'post_object_id',
        'meta_value' => $id
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();

            $status_order = get_field('status_order');

            if ($status_order != 'pending' && $status_order != 'canceled') {

                $get_numbers = get_field('client_order_numbers');

                $get_numbers = explode(',', $get_numbers);

                $numbers[] = $get_numbers;
            }
        } wp_reset_postdata();

        $numbers = array_merge([], ...$numbers);

        $numbers = array_unique($numbers);
    }

    return $numbers;
}

function get_post_user_selected_numbers($id) {

    $numbers = [];

    $args = array(
        'post_type' => 'post_product_order',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key' => 'post_object_id',
        'meta_value' => $id
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();


            $get_numbers = get_field('client_order_numbers');

            $get_numbers = explode(',', $get_numbers);

            $numbers[] = $get_numbers;
        } wp_reset_postdata();

        $numbers = array_merge([], ...$numbers);

        $numbers = array_unique($numbers);
    }

    return $numbers;
}

function check_value($number, $array) {


    $result = (in_array($number, $array) ? ' reserved' : false);

    return $result;
}

function check_value_status($number, $array) {


    $result = (in_array($number, $array) ? ' paid' : false);

    return $result;
}

add_filter('acf/prepare_field/name=client_order_numbers', 'disable_client_order_numbers');

function disable_client_order_numbers($field) {

    $field['disabled'] = true;

    return $field;
}

add_filter('acf/prepare_field/name=purchase_amount', 'disable_purchase_amount');

function disable_purchase_amount($field) {

    $field['disabled'] = true;

    return $field;
}

function wp_get_user_winner_order($id) {

    $client_name = false;

    $args = array(
        'post_type' => 'post_product_order',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'winner_order',
                'value' => 'yes'
            ),
            array(
                'key' => 'post_object_id',
                'value' => $id,
                'compare' => '='
            )
        )
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();

            $client_name = get_field('client_name');
        } wp_reset_postdata();
    }

    return $client_name;
}

function custom_get_post_user($id, $number) {

    $user = '';

    $args = array(
        'post_type' => 'post_product_order',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'post_object_id',
                'value' => $id,
                'compare' => '=',
            ),
            array(
                'key' => 'client_order_numbers',
                'value' => $number,
                'compare' => 'LIKE',
            ),
        ),
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();

            $get_status = get_field('status_order');

            $status = ($get_status == 'paid' || $get_status == 'completed' ? ' Pago por: ' : ' Reservado por: ');

            $user = 'Número ' . $number . $status . get_field('client_name');
        } wp_reset_postdata();
    }

    return $user;
}

function admin_head_input_readonly() {
    global $post_type;
    if ('post_product_order' != $post_type) : return;
    endif;
    ?>
    <style>
        span.canceled{background:red;color:#fff;line-height:2em;border-radius:4px;width:100%;display:block;text-align:center;max-width:70px}span.pending{background:#ff9800;color:#fff;border-radius: 4px;line-height:2em;width:100%;display:block;text-align:center;max-width:70px}span.paid,span.completed{background:#8bc34a;color:#fff;line-height:2em;border-radius:4px;width:100%;display:block;text-align:center;max-width:70px}
        #misc-publishing-actions,
        #minor-publishing-actions{
            display:none;}
        </style>
        <script> jQuery(document).ready(function ($) {
                $('#title').attr('disabled', 'disabled');
            });</script>
        <?php
    }

    add_action('admin_head', 'admin_head_input_readonly');


    add_action('acf/render_field_settings/type=text', 'add_readonly_and_disabled_to_text_field');

    function add_readonly_and_disabled_to_text_field($field) {
        acf_render_field_setting($field, array(
            'label' => __('Somente leitura?', 'acf'),
            'instructions' => '',
            'type' => 'true_false',
            'name' => 'readonly',
            'ui' => 1,
            'class' => 'acf-field-object-true-false-ui'
        ));

        acf_render_field_setting($field, array(
            'label' => __('Desativado?', 'acf'),
            'instructions' => '',
            'type' => 'true_false',
            'name' => 'disabled',
            'ui' => 1,
            'class' => 'acf-field-object-true-false-ui',
        ));
    }

    add_filter('post_row_actions', 'remove_bulk_actions', 10, 2);

    function remove_bulk_actions($unset_actions, $post) {
        global $current_screen;

        if ($current_screen->post_type != 'post_product_order')
            return $unset_actions;
        unset($unset_actions['inline hide-if-no-js']);

        return $unset_actions;
    }

    function get_currency_price() {

        global $post;

        $currency = get_field('pro_currency', 'option');
        $thousands_separator = get_field('thousands_separator', 'option');
        $decimal_separator = get_field('decimal_separator', 'option');
        $number_decimal_places = get_field('number_decimal_places', 'option');
        $currency_position = get_field('currency_position', 'option');
        $getCurrency = !empty($currency) ? $currency : 'R$';
        $getThousandsSeparator = !empty($thousands_separator) ? $thousands_separator : '.';
        $getDecimalSeparator = !empty($decimal_separator) ? $decimal_separator : ',';
        $getNumberDecimal = !empty($number_decimal_places) ? $number_decimal_places : 2;

        $valor1 = get_field('value_per_number', $post->ID);

        if (!empty($valor1)) {
            ?>

            <?php if (!empty($currency_position) && $currency_position === 'left'): ?>
                <li class="my-2 mr-3"><b>Valor Unitário:</b> <?php _e($getCurrency, 'rifapress'); ?><?php echo number_format($valor1, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></li>
            <?php elseif (!empty($currency_position) && $currency_position === 'left_space'): ?>
            <li class="my-2 mr-3"><b>Valor Unitário:</b> <?php _e($getCurrency, 'rifapress'); ?> <?php echo number_format($valor1, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></li>
        <?php elseif (!empty($currency_position) && $currency_position === 'right'): ?>
            <li class="my-2 mr-3"><b>Valor Unitário:</b> <?php echo number_format($valor1, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?><?php _e($getCurrency, 'rifapress'); ?></li>
        <?php elseif (!empty($currency_position) && $currency_position === 'right_space'): ?>
            <li class="my-2 mr-3"><b>Valor Unitário:</b> <?php echo number_format($valor1, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?> <?php _e($getCurrency, 'rifapress'); ?></li>
        <?php else: ?>
            <li class="my-2 mr-3"><b>Valor Unitário:</b> <?php _e($getCurrency, 'rifapress'); ?> <?php echo number_format($valor1, $getNumberDecimal, $getDecimalSeparator, $getThousandsSeparator); ?></li>
        <?php
        endif;
    }
}

function custom_get_date_of_draw($postId) {

    $date_of_draw = get_field('date_of_draw', $postId);

    return $date_of_draw;
}

function isInArray($values, $array) {

    $nambers = false;

    foreach ($values as $value) {

        if (in_array($value, $array)) {
            $nambers[] = $value;
        }
    }

    return $nambers;
}

function wpdocs_delete_products() {

    $args = array(
        'post_type' => 'post_product_order',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key' => 'status_order',
        'meta_value' => 'pending',
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();

            $days = get_post_meta(get_the_ID(), 'limit_order_remove', true);
            $today = strtotime(date('Ymd'));
            $getDate = get_the_date('Ymd');
            $fullDate = date('Ymd', strtotime('+' . $days . 'days', strtotime($getDate)));
            $checkDate = strtotime($fullDate);
            if (!empty($days) && $today > $checkDate) {

                wp_delete_post(get_the_ID(), true);
            }
        }

        wp_reset_postdata();
    }
}

add_action('init', 'wpdocs_delete_products');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
	
		'page_title' 	=> 'Métodos de pagamento',
		'menu_title'	=> 'Métodos de pagamento',
		'menu_slug' 	=> 'payment-settings',
		'capability'	=> 'edit_posts',
		'icon_url'     => 'dashicons-admin-generic',
		'redirect'		=> false,
	    'update_button'	=> __('Salvar alterações', 'acf'),
		'updated_message' => __("Alterações atualizadas", 'acf'),
		'position'		=> 9
	));
}
