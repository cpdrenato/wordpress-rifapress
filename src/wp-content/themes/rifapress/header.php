<!DOCTYPE html>
	<html <?php language_attributes(); ?>>
	<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<?php $pro_add_open_graph = get_field('enable_open_graph', 'option'); ?>
	<?php if (!empty($pro_add_open_graph)) { get_template_part('/includes/config-seo');} ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/cdn/html5shiv.js"></script>
	<![endif]-->   
	<?php wp_head(); ?>
	<!-- Favicon -->
	<?php if (get_field('pro_favicon', 'option') <> "") { ?>
	<link rel="shortcut icon" href="<?php echo get_field('pro_favicon', 'option'); ?>" />
	<?php } else { ?>
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.png" />
	<?php } ?>
	<script>if(performance.navigation.type == 2){location.reload(true)}</script>
	</head>
	<body <?php body_class(); ?>>
	<?php $logo = get_field('pro_logo', 'option'); ?>
	<nav class="navbar navbar-expand-lg shadow navbar-dark bg-dark bg_custum_menu">
	<div class="container">
	<?php if (!empty($logo)) { ?>
	<a class="navbar-brand navbar-brand-img" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
	<img src="<?php echo $logo; ?>" width="140" height="40" class="d-inline-block align-top" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
	</a>
	<?php }else{ ?>
	<a class="navbar-brand" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>
	<?php } ?>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
	aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Menu">
	<span class="navbar-toggler-icon"></span>
	</button>
	<?php if (has_nav_menu('main_menu')) { ?>
	<?php 
	wp_nav_menu( array(
	'theme_location'    => 'main_menu',
	'depth'             => 2,
	'container'         => 'div',
	'container_class'   => 'collapse navbar-collapse',
	'container_id'      => 'navbarNavDropdown',
	'menu_class'        => 'navbar-nav ml-md-auto text-uppercase font-weight-bold',
	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
	'walker'            => new WP_Bootstrap_Navwalker(),
	) );
	?>
	<?php } else { ?>	
	<div class="collapse navbar-collapse" id="navbarNavDropdown">				
	<ul class="navbar-nav">		
	<li class="nav-item active">
	<a class="nav-link" href="<?php echo home_url(); ?>">Home</a>
	</li>
	</ul>
	</div>
	<?php } ?>
	</div>
	</nav>