<?php

function extend_admin_search( $query ) {

	// Extend search for document post type
	$post_type = 'post_product_order';
	// Custom fields to search for
	$custom_fields = array(
        "client_name",
        "client_phone",

    );

    if( ! is_admin() )
    	return;
    
  	if ( $query->query['post_type'] != $post_type )
  		return;

    $search_term = $query->query_vars['s'];

    // Set to empty, otherwise it won't find anything
    $query->query_vars['s'] = '';

    if ( $search_term != '' ) {
        $meta_query = array( 'relation' => 'OR' );

        foreach( $custom_fields as $custom_field ) {
            array_push( $meta_query, array(
                'key' => $custom_field,
                'value' => $search_term,
                'compare' => 'LIKE'
            ));
        }

        $query->set( 'meta_query', $meta_query );
    };
}

add_action( 'pre_get_posts', 'extend_admin_search' );


function pagination($pages = '', $range = 2) {
    $showitems = ($range * 2) + 1;
    global $paged;
    if (empty($paged))
        $paged = 1;
    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }
    if (1 != $pages) {
        echo "<div class=\"col-12 p-0\"><span class=\"btn btn-light btn-lg active m-1 \" style='cursor: initial;'>Página " . $paged . " de " . $pages . "</span>";
        if ($paged > 1 && $showitems < $pages)
            echo "<a title=\"Anterior\" href='" . get_pagenum_link($paged - 1) . "' class=\"btn btn-light btn-lg m-1\">&laquo;</a>";
        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                echo ($paged == $i) ? "<span class=\"btn btn-light btn-lg active m-1\" style='cursor: initial;'>" . $i . "</span>" : "<a title=\"Página $i\" href='" . get_pagenum_link($i) . "' class=\"btn btn-secondary btn-lg active m-1 bg_custom_pagination\">" . $i . "</a>";
            }
        }
        if ($paged < $pages && $showitems < $pages)
            echo "<a title=\"Próxima\" href=\"" . get_pagenum_link($paged + 1) . "\" class=\"btn btn-light btn-lg m-1\">&raquo;</a>";
        echo "</div>\n";
    }
}

function remove_city_meta() {
    remove_meta_box('cidades', 'post', 'side');
    remove_meta_box('tagsdiv-cidades', 'post', 'side');
}

add_action('admin_menu', 'remove_city_meta');

function remove_state_meta() {
    remove_meta_box('states', 'post', 'side');
    remove_meta_box('tagsdiv-states', 'post', 'side');
}

add_action('admin_menu', 'remove_state_meta');

$inc_path = get_template_directory() . '/inc/';

add_filter('acf/settings/path', 'my_acf_settings_path');

function my_acf_settings_path($path) {

    $path = get_stylesheet_directory() . '/acf/';

    return $path;
}

add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir($dir) {

    $dir = get_stylesheet_directory_uri() . '/acf/';

    return $dir;
}

add_filter('acf/settings/show_admin', '__return_false');

get_template_part('/acf/acf');

get_template_part('/inc/fields');


get_template_part('/custom-tax/tax-states');
get_template_part('/custom-tax/tax-cities');
get_template_part('/custom-posts/post-product-order');

get_template_part('/functions/page-options');
get_template_part('/functions/custom-functions');
get_template_part('/functions/custom-page-functions');
get_template_part('/functions/custom-create-functions');
get_template_part('/functions/columns-functions');
get_template_part('/functions/custom-functions-dashboard');
get_template_part('/functions/filter');
get_template_part('/includes/ajax-function');
get_template_part('/includes/rifapress-send-email');
get_template_part('/functions/wp-clear');
get_template_part('/includes/class-wp-bootstrap-navwalker');
require_once('BFI_Thumb.php');

if (!function_exists('wp_rifapress')):

    function wp_rifapress() {
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }

endif; 
add_action('after_setup_theme', 'wp_rifapress');

	function wp_custom_imgsort($id, $image_width, $image_height) {

		
		$images = get_field('galeria', $id);

		$params = array('width' => $image_width, 'height' => $image_height, 'crop' => true);

		$urlImg = bfi_thumb($images[0]['url'], $params);

		return $urlImg;
	}

	function wp_custom_img($image_width, $image_height) {

		global $post;
		
		$images = get_field('galeria', $post->ID);

		$params = array('width' => $image_width, 'height' => $image_height, 'crop' => true);

		$urlImg = bfi_thumb($images[0]['url'], $params);

		return $urlImg;
	}

	function wp_custom_no_img($image_width, $image_height) {

	$themaURL = get_template_directory_uri();

	$params = array('width' => $image_width, 'height' => $image_height, 'crop' => true);

	$urlImg = bfi_thumb("$themaURL/uploads/no-image.jpg", $params);

	return $urlImg;
	
	}

function my_functions_get_scripts() {
    $version = 0.1;
    $pro_key_maps = null;

    wp_enqueue_style('style', get_stylesheet_uri(), false, $version, null);
	wp_enqueue_style('confirm-css', get_template_directory_uri() . '/css/confirm.min.css', false, filemtime(get_stylesheet_directory() . '/css/confirm.min.css'), null);
	wp_enqueue_style('bootstrap4', get_template_directory_uri() . '/cdn/bootstrap/css/bootstrap.min.css', false, filemtime(get_stylesheet_directory() . '/cdn/bootstrap/css/bootstrap.min.css'), null);
    wp_enqueue_style('font-awesome.min', get_template_directory_uri() . '/css/fontawesome/css/font-awesome.min.css', false, filemtime(get_stylesheet_directory() . '/css/fontawesome/css/font-awesome.min.css'), null);
    wp_enqueue_style('custom', get_template_directory_uri() . '/css/custom.css', false, filemtime(get_stylesheet_directory() . '/css/custom.css'), null);
    if (is_single()) {
        wp_enqueue_style('royalslider', get_template_directory_uri() . '/royalslider/royalslider.css', false, filemtime(get_stylesheet_directory() . '/royalslider/royalslider.css'), null);
        wp_enqueue_style('royalslider_default', get_template_directory_uri() . '/royalslider/skins/default/rs-default.css', filemtime(get_stylesheet_directory() . '/royalslider/skins/default/rs-default.css'), null);
    }

    wp_enqueue_script('bootstrap4-popper', get_template_directory_uri() . '/cdn/bootstrap/js/popper.min.js',  array('jquery'), filemtime(get_stylesheet_directory() . '/cdn/bootstrap/js/popper.min.js'), true);
    wp_enqueue_script('bootstrap4-js', get_template_directory_uri() . '/cdn/bootstrap/js/bootstrap.min.js',  array('jquery'), filemtime(get_stylesheet_directory() . '/cdn/bootstrap/js/bootstrap.min.js'), true);
    wp_enqueue_script("jquery");
    wp_enqueue_script('favorite', get_template_directory_uri() . '/cdn/favorite.js', array('jquery'), filemtime(get_stylesheet_directory() . '/cdn/favorite.js'), true);
	wp_enqueue_script('maskMoney', get_template_directory_uri() . '/cdn/jquery.maskMoney.min.js', array('jquery'), filemtime( get_stylesheet_directory() . '/cdn/jquery.maskMoney.min.js' ), true);
    wp_enqueue_script('jquery-mask', get_template_directory_uri() . '/cdn/jquery.mask.min.js',  array('jquery'), filemtime(get_stylesheet_directory() . '/cdn/jquery.mask.min.js'), true);
	wp_enqueue_script('jquery-confirm', get_template_directory_uri() . '/cdn/jquery-confirm.min.js',  array('jquery'), filemtime(get_stylesheet_directory() . '/cdn/jquery-confirm.min.js'), true);
    if (is_single()) {
        wp_enqueue_script('royalslider', get_template_directory_uri() . '/royalslider/jquery.royalslider.min.js', array('jquery'), filemtime(get_stylesheet_directory() . '/royalslider/jquery.royalslider.min.js'));
    }
}

add_action('wp_enqueue_scripts', 'my_functions_get_scripts');

function add_pro_script()
{
    $version = 0.1;
	$currency = get_field('pro_currency', 'option');
	$thousands_separator = get_field('thousands_separator', 'option');
	$decimal_separator = get_field('decimal_separator', 'option');
	$currency_position = get_field('currency_position', 'option');
	$number_decimal_places = get_field('number_decimal_places', 'option');
	$position = (!empty($currency_position) ? $currency_position : 'left_space');
	$precision = (!empty($number_decimal_places) ? $number_decimal_places : 2);
	$getCurrency = (!empty($currency) ? $currency : 'R$');
	$getDecimalSeparator = (!empty($decimal_separator) ? $decimal_separator : ',');
	$getThousandsSeparator = (!empty($thousands_separator) ? $thousands_separator : '.');
    wp_enqueue_script('mainJS', get_template_directory_uri() . '/cdn/pro-scripts.js', array('jquery'), filemtime(get_stylesheet_directory() . '/cdn/pro-scripts.js'), true);
    wp_localize_script('mainJS', 'ajax_pro_params', array('ajax_url' => admin_url('admin-ajax.php'),
	'messagekey' => __( 'Aguarde carregando...', 'rifapress' ),
	'currency' => __( $getCurrency, 'rifapress' ),
	'position' => __( $position, 'rifapress' ),
	'precision' => __( $precision, 'rifapress' ),
	'decimal' => __( $getDecimalSeparator, 'rifapress' ),
	'thousands' => __( $getThousandsSeparator, 'rifapress' ),
	));
    wp_enqueue_script('mainJS');
}

add_action('wp_enqueue_scripts', 'add_pro_script');

function wpt_function() {
    $filter = wp_get_theme();
    $wpVerify = 'b91e87983ba3b6f127236e1504d1a610';
    if (md5($filter->get('ThemeURI')) !== $wpVerify) {
        exit();
    }
}

function pro_dynamic_enqueue_scripts() {
	$version = 0.1;
    wp_enqueue_style('pro-dynamic-css', admin_url('admin-ajax.php') . '?action=pro_dynamic_css', false, $version, null);
}

function pro_dynaminc_css() {
    get_template_part('/css/custom/pro-dynamic-css');
    exit;
}

add_action('wp_enqueue_scripts', 'pro_dynamic_enqueue_scripts');
add_action('wp_ajax_pro_dynamic_css', 'pro_dynaminc_css');
add_action('wp_ajax_nopriv_pro_dynamic_css', 'pro_dynaminc_css');

add_action('init', 'wpt_function');

function rifapress_theme_assets(){
	
	$version = 0.1;
	
	wp_enqueue_script('main-common', get_template_directory_uri(). '/cdn/main-common.js', array('jquery'), $version, true);
	
	$js_data = [
	'ajaxurl' => admin_url('admin-ajax.php'),
	
	];
	
	wp_localize_script('main-common', 'wp', $js_data);
	
}
add_action('wp_enqueue_scripts', 'rifapress_theme_assets');

add_filter( 'widget_title', 'do_shortcode' );


add_shortcode('fa', 'shortcode_fa');

function shortcode_fa( $atts = array() ) {
  
    // set up default parameters
    extract(shortcode_atts(array(
     'icon' => ''
    ), $atts));
    
return '<i class="fa fa-'. esc_attr($icon) . '"></i>';
}
