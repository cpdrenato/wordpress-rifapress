<?php
function cptui_register_my_cpts_post_product_order() {

	/**
	 * Post Type: Product Order.
	 */

	$labels = array(
		"name" => __( "Pedidos", "rifapress" ),
		"singular_name" => __( "Pedido", "rifapress" ),
		"edit_item" => __( "Editar Pedido", "rifapress" ),
		"search_items" => __( "Pesquisar pedido", "rifapress" ),
		"not_found" => __( "Nenhum pedido encontrado", "rifapress" ),
		"not_found_in_trash" => __( "Nenhum pedido encontrado na lixeira", "rifapress" ),
	);

	$args = array(
		"label" => __( "Pedidos", "rifapress" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => false,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
        "capabilities"   => array( 'create_posts' => 'do_not_allow',),
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "post_product_order", "with_front" => true ),
		"query_var" => false,
		"menu_position" => 5,
		"menu_icon" => "dashicons-cart",
		"supports" => array( "title" ),
	);

	register_post_type( "post_product_order", $args );
}

add_action( 'init', 'cptui_register_my_cpts_post_product_order' );