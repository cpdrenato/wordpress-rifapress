	<footer class="bg-dark text-white bg_custom_main_footer">
    <div class="container py-4">
        <div class="row">
		<?php get_sidebar('footer'); ?>
        </div>
    </div>
<div class="container-fluid bg-success social_footer border-bottom border-white">
<div class="main_social">
<ul class="list-unstyled d-flex flex-wrap justify-content-center social m-0 py-2">
<?php
$whatsapp = get_field('number_whatsapp', 'option'); 
$facebook = get_field('facebook', 'option'); 
$instagram = get_field('instagram', 'option'); 
$twitter = get_field('twitter', 'option'); 
$youtube = get_field('youtube', 'option'); 


$phone = (!empty($whatsapp) ? $whatsapp : false);
$fb = (!empty($facebook) ? $facebook : false);
$insta = (!empty($instagram) ? $instagram : false);
$twit = (!empty($twitter) ? $twitter : false);
$yt = (!empty($youtube) ? $youtube : false);


?>
<?php if ($fb): ?>
<li class="ml-3 my-2"><a class="facebook" target="_blank" href="<?php echo $facebook; ?>" title="Facebook"><i class="fa fa-facebook"></i></a></li>
<?php endif; ?>
<?php if ($insta): ?>
<li class="ml-3 my-2"><a class="instagram" target="_blank" href="<?php echo $instagram; ?>" title="Instagram"><i class="fa fa-instagram"></i></a></li>
<?php endif; ?>
<?php if ($twit): ?>
<li class="ml-3 my-2"><a class="twitter" target="_blank" href="<?php echo $twitter ;?>" title="Twitter"><i class="fa fa-twitter"></i></a></li> 
<?php endif; ?>
<?php if ($yt): ?>
<li class="ml-3 my-2"><a class="youtube" target="_blank" href="<?php echo $youtube ; ?>" title="Youtube"><i class="fa fa-play"></i></a></li> 
<?php endif; ?>
<?php if ($phone): ?>
<li class="ml-3 my-2"><a class="whatsapp" target="_blank" href="https://api.whatsapp.com/send?l=pt_br&amp;phone=<?php echo $phone; ?>" title="WhatsApp"><i class="fa fa-whatsapp"></i></a></li>
<?php endif; ?>
</ul>
</div>
</div>
    <div class="bg-success text-white text-center py-3 bg_custom_secondary_footer">
	   <?php if (get_field('pro_copyright', 'option') != ''): ?>
        <p class="mb-0"><?php echo get_field('pro_copyright', 'option'); ?></p>
		<?php else: ?>
		<p class="mb-0">© <?php echo date('Y'); ?>. Todos os direitos reservados</p>
		<?php endif; ?>
    </div>
    <footer>
<script>
WebFontConfig={google:{families:["Titillium+Web:300,400,600:latin","Days+One","Roboto","Lato","Source+Sans+Pro","Dosis","Open+Sans"]}};(function(){var a=document.createElement("script");a.src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js";a.type="text/javascript";a.async="true";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();
</script>
            <style>
            * input, textarea {

                -webkit-touch-callout: none;
                /*-webkit-user-select: none;*/
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
            body {

                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
        </style>
            <script type="text/javascript">
            jQuery(document).ready(function ($) {
                jQuery("a").each(function (i, el) {
                    var href_value = el.href;
                    if (/\.(jpg|png|gif)$/.test(href_value)) {
                        jQuery(this).prop('href', '#');
                    }


                });


            });</script>
<?php wp_footer(); ?>
</body>
</html>