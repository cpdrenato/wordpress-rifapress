	<?php get_header(); ?>
	<div class="container-fluid mt-5">
	<?php
	$value_per_number = get_field('value_per_number');
	$images = get_field('galeria');
	$number_drawn = get_field('number_drawn');
	$validity_days = get_field('limit_days_remove');
	$inform_date = get_field('inform_date_the_draw');
	$date_of_draw = get_field('date_of_draw', false, false);
	$arrValues = custom_get_post_selected_numbers($post->ID);
	$today = strtotime(date('Ymd'));		
	$checkDrawDate = strtotime($date_of_draw);
	$payment_data = get_field('_payment_data'); 
	$whatsapp = get_field('number_whatsapp', 'option'); 
	?>
	<div class="container">
	<div class="my-3 border-bottom">
	<h1 class="h4 text-center color_tilte_single"><?php the_title(); ?></h1>
	</div>
	<div class="mb-2">
	<?php if (!empty($images)): ?>
	<div id="property_gallery" class="royalSlider rsDefault">
	<?php foreach ($images as $image): ?>
	<img class="rsImg" src="<?php echo $image['url']; ?>" data-rsBigImg="<?php echo $image['url']; ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
	<?php endforeach; ?>
	</div>
	<script>
	jQuery(document).ready(function(b){var d=b("#property_gallery").royalSlider({fullscreen:{enabled:true,nativeFS:true},controlNavigation:false,autoScaleSlider:true,autoScaleSliderWidth:1280,autoScaleSliderHeight:720,loop:false,loopRewind:true,imageScaleMode:"fit",navigateByClick:true,numImagesToPreload:2,arrowsNav:true,arrowsNavAutoHide:true,arrowsNavHideOnTouch:true,keyboardNavEnabled:true,fadeinLoadedSlide:true,globalCaption:false,globalCaptionInside:false,});b(".rsContainer").on("touchmove touchend",function(){});var e=d.data("royalSlider");var a=b('<div class="rsSlideCount"></div>').appendTo(d);function c(){a.html((e.currSlideId+1)+" de "+e.numSlides)}e.ev.on("rsAfterSlideChange",c);c()});
	</script>
	<?php else: ?>               
	<img class="img-fluid" src="<?php echo wp_custom_no_img(1280, 720); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
	<?php endif; ?>
	</div>
	<div style="position: relative;">
	<span class="processing"><?php _e('Aguarde processando...', 'rifapress'); ?></span>
	<?php echo pro_get_button_favorite(); ?>
	</div>
	<!-- Tab panes -->
	<ul class="nav nav-tabs mt-2 bg_nav" role="tablist">
	<?php if($payment_data){ ?>
	<li class="nav-item">
	<a class="nav-link" href="#dados" role="tab" data-toggle="tab">Dados para pagamento</a>
	</li>
	<?php } ?>
	<li class="nav-item">
	<a class="nav-link" href="#descricao" role="tab" data-toggle="tab"> Regulamento/Descrição</a>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="#references" role="tab" data-toggle="tab">Informações do item</a>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="#contato" role="tab" data-toggle="tab">Contato</a>
	</li>
	</ul>
	<!-- Tab panes -->
	<div class="tab-content">
	<?php if($payment_data){ ?>
	<?php if( have_rows('payment_content') ){ ?>
	<div role="tabpanel" class="tab-pane fade" id="dados">
	<div class="card card-body">
	<div class="row">
	<?php while ( have_rows('payment_content') ) { the_row(); 
	$payment_image = get_sub_field('add_payment_option_image');
	$payment_title = get_sub_field('add_payment_title');
	$text_field = get_sub_field('_add_text_field');
	?>
	<div class="col-md-6 col-lg-2 text-center">
	<p class=""><img class="img-fluid" width="115" height="115" src="<?php echo $payment_image ?>"></p>
	<p class="h5"><?php echo $payment_title ?></p>
	<?php if( !empty($text_field) && have_rows('_add_text_field') ){ 
	while ( have_rows('_add_text_field') ) { the_row(); 
	$payment_option_text = get_sub_field('payment_option_text'); ?>
	<p class=""><?php echo $payment_option_text; ?></p>
	<?php } } ?>
	</div>
	<?php } ?>
	</div>
	</div>
	</div>
	<?php } } ?>
	<div role="tabpanel" class="tab-pane fade" id="descricao">		
	<div class="bg_custom_info">
	<div class="card card-body bg_custom_desc">
	<p>
	<?php
	if (have_posts()) : while (have_posts()) : the_post(); 
	
	
				  // tracking code
				if (get_post_type($post->ID) == 'post') {
					update_post_meta($post->ID, 'post_last_viewed', current_time('mysql'));
					update_post_meta($post->ID, 'post_visitor_ip', $_SERVER['REMOTE_ADDR']);
				}
				
		the_content(); ?>
	<?php endwhile; endif; ?>
	</p>
	</div>	
	</div>
	</div>
	<div role="tabpanel" class="tab-pane fade" id="references">					
	<div class="card card-body mb-1">
	<ul class="list-unstyled d-flex flex-wrap justify-content-between py-2">
	<?php echo get_currency_price(); ?>
	<?php 
	$total_numbers = get_field('number_of_numbers');?>
	<?php if ($total_numbers): ?>
	<li class="my-2 mr-3"><b>Total:</b> <?php echo $total_numbers; ?></li>
	<?php endif; ?>
	<?php 
	$reserved = (!empty($arrValues) ? sizeof($arrValues) : 0);
	$number_of_numbers = get_field('number_of_numbers');
	$available = intval($number_of_numbers) - $reserved;
	$textAvailable = $available > 1 ? 'Disponíveis' : 'Disponível';						
	?>	
	<?php if ($reserved): ?>
	<li class="my-2 mr-3"><b>Reservaos:</b> <?php echo $reserved; ?></li>
	<?php endif; ?>
	<li class="my-2 mr-3"><b><?php echo $textAvailable; ?>:</b> <?php echo $available; ?></li>
	<?php $reward = get_field('reward');
	if ($reward): ?>
	<li class="my-2 mr-3"><b>Prêmio:</b> <?php echo $reward; ?></li>
	<?php endif; ?>
	<?php if ($inform_date): 
	$drawDate = get_field('date_of_draw'); ?>
	<li class="my-2 mr-3"><b>Data do sorteio:</b> <?php echo $drawDate; ?></li>
	<?php endif; ?>
	<?php $city = get_field('raffle_city');
	if ($city): ?>
	<li class="my-2 mr-3"><b>Cidade:</b> <?php echo $city->name; ?></li>
	<?php endif; ?>
	<?php $state = get_field('raffle_state');
	if ($state): ?>
	<li class="my-2 mr-3"><b>Estado:</b> <?php echo $state->name; ?></li>
	<?php endif; ?>
	<li class="my-2 mr-3"><?php echo get_the_term_list($post->ID, 'category', ' <b>Categoria:</b> ', ', '); ?></li>
	</ul>
	</div>
	<?php 
	$meta_for_sale = get_field('for_sale');
	$percentage = (!empty($reserved) ? ($reserved / $number_of_numbers) * 100 : 0 ); 
	?>
	<?php if(!empty($meta_for_sale)) { ?>
	<div class="progress" style="height: 28px;">
	<div class="progress-bar bg-success p-2 text-dark font-weight-bold" role="progressbar" style="width:<?php echo $percentage;?>%;" aria-valuenow="<?php echo $percentage;?>" aria-valuemin="0" aria-valuemax="100"><?php echo $percentage;?>% da meta de <?php echo $meta_for_sale; ?>%</div>
	</div>
	<?php } ?>
	</div>
	<div role="tabpanel" class="tab-pane fade" id="contato">	
	<div class="card card-body">
	<?php if( have_rows('main_contact_field') ){ ?>
	<div class="row">
	<?php while ( have_rows('main_contact_field') ) { the_row(); 
	$contact_title = get_sub_field('contact_title');
	$contact_field = get_sub_field('contact_field');
	?>
	<div class="col-md-6 col-lg-3 text-center">
	<p class="h5"><?php echo $contact_title ?></p>
	<p class=""><?php echo $contact_field; ?></p>
	</div>
	<?php }  ?>
	</div>
	<?php } ?>
	</div>  
	</div>
	</div>
	<!-- Tab panes -->
	</div>
	<div class="bg_custom_draw mb-5">
	<?php
	$statusPaid = custom_get_selected_numbers_status($post->ID);	
	$paid = count($statusPaid);			
	$setStatuDate = (!empty($inform_date) && $today > $checkDrawDate ? 'disabled' : false);
	$setStatu = (!empty($inform_date) &&  $today > $checkDrawDate ? ' disabled' : false);
	$name = wp_get_user_winner_order($post->ID);
	$autor = get_the_author_meta( 'ID' );
	$fullDateLimit = date('Ymd', strtotime('+'.$validity_days. 'days'));
	$fullDateLimitBr = date('d/m/Y', strtotime($fullDateLimit));
	$phone = (!empty($whatsapp) ? $whatsapp : false);
	$message = 'Gostaria de saber sobre a rifa '.$post->post_title.'. Obrigado(a).';
	$urlencode = 'https://api.whatsapp.com/send?l=pt_br&phone='.urlencode($phone).'&text='.urlencode($message);
	?>
	<div class="card-body">
	<?php if($setStatuDate == 'disabled') { ?>
	<div class="alert alert-danger text-center text-uppercase">O sorteio dessa rifa já foi realizado! <?php if(!empty($number_drawn)) { echo 'O número sorteado foi: ' .$number_drawn; } ?> <?php if(!empty($name)) { echo ' - Ganhador: ' .$name; } ?></div>
	<?php } ?>
	<div class="main-selected-numbers">
	<span class="js-value"></span>
	<button type="button" id="" title="Clique para reservar" class="btn btn-success btn-lg btn-block btnSubmit buy_raffle" data-toggle="modal" data-target="#numberReservation">Reservar número(s)</button>
	<div class="clear"></div>
	</div>
	<ul class="list-unstyled caption-numbers d-flex flex-wrap">
	<li class="available"><b><?php echo $textAvailable.' '.$available; ?></b></li>   
	<li class="reserved"><b>RESERVADOS <?php echo $reserved - $paid; ?></b></li>   
	<li class="paid"><b>PAGO <?php echo $paid; ?></b></li>
	<li class="btn-your-purchase" style="display:none"><b>SEU(S) NÚMERO(S)</b></li>  
	<li class="send-receipt" data-title="<?php echo $post->post_title; ?>" data-toggle="modal" data-raffleid="1017" data-target="#sendreceipt"><b>Enviar comprovante</b></li>                    
	</ul>
	<ul class="list-unstyled d-flex flex-wrap mb-0 list_card">
	<?php if($setStatuDate != 'disabled') { ?>
	<?php 
	$totalNumber = $number_of_numbers - 1;
	for ($i = 0; $i <= $totalNumber; $i++) { 
	$number = $i < 10 ? '00' .$i : ($i < 100 ? '0'.$i : $i); 
	?>
	<li><a href="#" id="<?php echo $number; ?>" class="btn rifa_number<?php echo check_value($number, $arrValues); ?><?php echo check_value_status($number, $statusPaid); ?><?php echo $setStatu; ?>" data-toggle="tooltip" data-rid="<?php echo $post->ID?>" title="<?php echo custom_get_post_user($post->ID, $number);?>" data-price="<?php echo $value_per_number; ?>"><?php echo $number; ?></a></li>
	<?php } ?>
	<?php }else{ ?>
	<?php 
	$totalNumber = $number_of_numbers - 1;
	for ($i = 0; $i <= $totalNumber; $i++) { 
	$number = $i < 10 ? '00' .$i : ($i < 100 ? '0'.$i : $i); 
	?>
	<li><span id="<?php echo $number; ?>" class="btn rifa_number<?php echo check_value($number, $arrValues); ?><?php echo check_value_status($number, $statusPaid); ?><?php echo $setStatu; ?>" data-toggle="tooltip" data-rid="<?php echo $post->ID?>" title="<?php echo custom_get_post_user($post->ID, $number);?>" data-price="<?php echo $value_per_number; ?>"><?php echo $number; ?></span></li>
	<?php } ?>
	<?php } ?>
	</ul>
	</div>
	<div class="modal fade mt-1" id="numberReservation" tabindex="-1" role="dialog" aria-labelledby="numberReservationLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
	<div class="modal-header">
	<h5 class="modal-title" id="numberReservationLabel">Reserva de número</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
	<span aria-hidden="true">&times;</span>
	</button>
	</div>
	<div class="modal-body">
	<p style="word-wrap:break-word;">Número(s) selecionado(s) <span class="font-weight-bold numbers"></span></p>
	<p>Valor a pagar <span class="font-weight-bold value"></span></p>
	<p>Por favor, preencha os dados abaixo para poder reservar o(s) número(s).</p>
	<form method="post" class="" data-fulldate="<?php echo $fullDateLimitBr; ?>" id="paymentForm" novalidate>
	<input type="hidden" name="selectednumbers" id="selected-numbers" value="">
	<input type="hidden" id="currency">
	<input type="hidden" name="pid" id="pid" value="<?php echo $post->ID; ?>">
	<input type="hidden" name="uid" id="uid" value="<?php echo $autor; ?>">
	<input type="hidden" name="vlitem" id="vlitem" value="<?php echo $value_per_number; ?>">
	<input type="hidden" name="vdays" id="vdays" value="<?php echo $validity_days; ?>">
	<input type="hidden" name="qtd" id="qtd" value="">
	<input type="hidden" name="ptitle" id="ptitle" value="<?php echo $post->post_title; ?>">
	<input type="hidden" name="vtotal" id="vtotal" value="">
	<div class="form-group">
	<label for="name" class="col-form-label">Nome completo:</label>
	<input type="text" name="name" class="form-control form-control-lg" id="name" placeholder="Digite o nome completo" title="Preencha o campo Nome Completo" minlength="10" required>
	</div>
	<div class="form-group">
	<label for="celular" class="col-form-label">Celular:</label>
	<input type="tel" name="phone" class="form-control form-control-lg js_phone" placeholder="Digite o telefone com DDD" id="celular" required>
	</div>
	<div class="form-group form-check">
	<div class="custom-control custom-checkbox my-1 mr-sm-2">
	<input type="checkbox" name="termUse" class="custom-control-input" id="termUse" checked required>
	<label class="custom-control-label" for="termUse">Reservando seu(s) número(s), você declara que leu e concorda com nossos <a href="<?php echo site_url('/termos-de-uso'); ?>" target="_blank" title="Termos de Uso">Termos de Uso.</a></label>
	</div>
	</div>
	<?php wp_nonce_field('action_create_post', 'field_create_post'); ?>
	<div class="modal-footer">
	<button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
	<button type="submit" id="btnSubmit" title="Clique para reservar" class="btn btn-success btn-block">Reservar</button>
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div> 
	<a class="d-none contact-whatsapp" data-title="<?php $post->post_title; ?>" href="<?php echo $urlencode; ?>" target="_blank" rel="noopener">Contato</a>	
	<div class="main-selected-numbers" style="position:sticky; bottom: 0; z-index: 1030;">
	<span class="js-value shadow"></span>
	<button type="button" title="Clique para reservar" class="btn btn-success btn-lg btn-block btnSubmit buy_raffle" data-toggle="modal" data-target="#numberReservation">Reservar número(s)</button>
	</div>
	
	<div class="modal fade mt-3" id="sendreceipt" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title font-weight-bold text-cente" id="TituloModalCentralizado">Envio de comprovante</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div class="main_content_action">
<h5 class="font-weight-bold text-center mb-3 raffle_title"></h5>
<a href="<?php echo $urlencode; ?>" class="btn btn-success btn-lg btn-block mb-3 text-uppercase whats_action" target="_blank" rel="noopener"><i class="fa fa-whatsapp"></i> WhatsApp</a>
<button type="button" class="btn btn-primary btn-lg btn-block text-uppercase send_email custom_bg_btn"><i class="fa fa-envelope-o"></i> Enviar email</button>
</div>
<div class="mt-3 form_send_email" style="display:none;">
    <?php echo do_shortcode('[contact-form-7 id="992" title="Envio de comprovante de pagamento"]'); ?>
</div>
</div>
</div>
</div>
</div></div>

	
	<?php get_footer();