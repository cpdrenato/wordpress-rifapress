<?php get_header(); 

	/*

	*

	* Template Name: Sorteios

	*

	*/
?>
	<div class="container">
	<?php $home_page_title = get_field('home_page_title', 'option'); ?>
	<?php if($home_page_title){ ?>
	<div class="my-4">
	<h2 class="text-center text-uppercase h4 color_custom_title_featured"><?php echo $home_page_title; ?></h2>
	</div>
	<?php } ?>
	<?php
	$args = array('post_type' => 'post',
	'ignore_sticky_posts' => true);
	$query = new WP_Query($args);
	?>
	<?php if ($query->have_posts()){ ?>
	<div class="row my-3 main_row">
	<?php $total = $query->found_posts; 
	$postCount = $query->post_count;
	while ($query->have_posts()) : $query->the_post();
	get_template_part('template-parts/home-content');
	endwhile;?>
	</div>
	<?php if( $total > $postCount ){ ?>
	<div class="row text-center">
	<div class="col">
	<button type="button" class="btn btn-outline-dark btn-lg mx-auto mt-4 btn_custom load_more" tabindex="-1" role="button" aria-disabled="true" data-page="1">Ver Mais</button>
	</div>
	</div>
	<?php } ?>
	<?php wp_reset_postdata(); ?>
	<?php }else{ ?>
	<div class="row my-3 main_row">
	<div class="col mb-3">
	<?php get_template_part('template-parts/home-content', 'none'); ?>
	</div>
	</div>
	<?php } ?>
	</div>
	<?php get_footer();