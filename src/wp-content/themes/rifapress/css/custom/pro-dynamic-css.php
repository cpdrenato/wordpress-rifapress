<?php

header("Content-type: text/css; charset: UTF-8");

$font_titles = get_field('font_titles_h1', 'option');
$size_font_title = get_field('size_font_title', 'option');
$font_general = get_field('font_general', 'option');

$menu_bg = get_field('menu_color', 'option');
$bg_main_submenu = get_field('bg_main_submenu', 'option');
$menu_color_link = get_field('menu_color_text', 'option');

$slide_title_color = get_field('color_slide_link', 'option');
$bg_filter = get_field('bg_search_filter', 'option');
$btn_search = get_field('color_search_button', 'option');
$color_text_search_button = get_field('color_text_search_button', 'option');

$featured_raffle_title = get_field('featured_raffle_title_text_color', 'option');

$color_text_ticket_price = get_field('color_ticket_price', 'option');
$color_title_raffles = get_field('color_title_raffles', 'option');

$color_title_raffles_single = get_field('color_title_raffles_single', 'option');
$background_gallery_images = get_field('background_gallery_images', 'option');
$bg_details_single = get_field('bg_details_single', 'option');
$color_details_single = get_field('color_details_single', 'option');
$all_page_title_color = get_field('all_page_title_color', 'option');

$bg_pagination = get_field('color_background_pagination', 'option');
$color_text_pagination = get_field('color_text_pagination', 'option');
$form_buttons_background = get_field('form_buttons_background', 'option');
$form_buttons_color_text = get_field('form_buttons_color_text', 'option');

$main_footer = get_field('footer_color', 'option');
$footer_text_color = get_field('footer_text_color', 'option');
$bg_secondary_footer = get_field('bg_secondary_footer', 'option');
$secondary_footer_text_color = get_field('secondary_footer_text_color', 'option');
$footer_and_sidebar_link_colors = get_field('footer_and_sidebar_link_colors', 'option');
?>
<?php
//fontes
if (!empty($font_titles) && $font_titles != 'Default') { ?>
    h2, a, ul li a, span{font-family: '<?php echo $font_titles; ?>', sans-serif;}
<?php } ?>
<?php if (!empty($size_font_title) && $size_font_title != '16px') { ?>
    h1, h2 {font-size:<?php echo $size_font_title; ?>;}
<?php } ?>
<?php
if (!empty($font_general) && $font_general != 'Default') { ?>
    a, h3, h4, h5, h6, li, span, input, select, textarea, p, label, div {font-family: '<?php echo $font_general; ?>', sans-serif;}
<?php } ?>
<?php if (!empty($menu_bg)): ?>
.bg_custum_menu{background-color:<?php echo $menu_bg; ?>!important;}
<?php endif; ?>
<?php if (!empty($bg_main_submenu)): ?>
.bg_custum_menu .dropdown-menu{background-color:<?php echo $bg_main_submenu; ?>!important;}
<?php endif; ?>
<?php if (!empty($menu_color_link)): ?>
.bg_custum_menu a{color:<?php echo $menu_color_link; ?>!important;}
<?php endif; ?>
<?php if (!empty($slide_title_color)): ?>
.color_custom_title_slide {color:<?php echo $slide_title_color; ?>!important;}
<?php endif; ?>
<?php if (!empty($bg_filter)): ?>
.bg_custom_filter{background-color:<?php echo $bg_filter; ?>!important;}
<?php endif; ?>
<?php if (!empty($btn_search)): ?>
.bg_custom_filter .btn{background-color:<?php echo $btn_search; ?>!important;}
<?php endif; ?>
<?php if (!empty($btn_search)): ?>
.bg_custom_filter .btn{border-color:<?php echo $btn_search; ?>!important;}
<?php endif; ?>
<?php if (!empty($color_text_search_button)): ?>
.bg_custom_filter .btn{color:<?php echo $color_text_search_button; ?>!important;}
<?php endif; ?>
<?php if (!empty($featured_raffle_title)): ?>
.color_custom_title_featured, .color_custom_title, span.result_number, span.text_result {color:<?php echo $featured_raffle_title; ?>!important;}
<?php endif; ?>
<?php if (!empty($color_text_ticket_price)): ?>
.bg_custom_price{color:<?php echo $color_text_ticket_price; ?>!important;}
<?php endif; ?>
<?php if (!empty($color_title_raffles)): ?>
.color_main_title {color:<?php echo $color_title_raffles; ?>!important;}
<?php endif; ?>
<?php if (!empty($color_title_raffles_single)): ?>
.color_tilte_single {color:<?php echo $color_title_raffles_single; ?>!important;}
<?php endif; ?>
<?php if (!empty($all_page_title_color)): ?>
.page-template h2, .page-template-default h2{color:<?php echo $all_page_title_color; ?>!important;}
<?php endif; ?>
<?php if (!empty($background_gallery_images)): ?>
.rsDefault, .rsDefault .rsOverflow, .rsDefault .rsSlide, .rsDefault .rsVideoFrameHolder, .rsDefault .rsThumbs{background:<?php echo $background_gallery_images; ?>!important;}
<?php endif; ?>
<?php if (!empty($bg_details_single)): ?>
.bg_nav{background-color:<?php echo $bg_details_single; ?>!important;}
<?php endif; ?>
<?php if (!empty($color_details_single)): ?>
.bg_nav a{color:<?php echo $color_details_single; ?>!important;}
<?php endif; ?>
<?php if (!empty($bg_details_single) && !empty($color_details_single)): ?>
.bg_nav a.active{background-color:<?php echo $color_details_single; ?>!important; color:<?php echo $bg_details_single; ?>!important;}
<?php endif; ?>
<?php if (!empty($bg_pagination)): ?>
.bg_custom_pagination{background-color:<?php echo $bg_pagination; ?>!important; border-color:<?php echo $bg_pagination; ?>!important;}
<?php endif; ?>
<?php if (!empty($color_text_pagination)): ?>
.bg_custom_pagination{color:<?php echo $color_text_pagination; ?>!important;}
<?php endif; ?>
<?php if (!empty($form_buttons_background)): ?>
form button[type="submit"], form input[type="submit"], .btn_custom{background-color:<?php echo $form_buttons_background; ?>!important; border-color:<?php echo $form_buttons_background; ?>!important;}
<?php endif; ?>
<?php if (!empty($form_buttons_color_text)): ?>
form button[type="submit"], form input[type="submit"], .btn_custom{color:<?php echo $form_buttons_color_text; ?>!important;}
<?php endif; ?>
<?php if (!empty($main_footer)): ?>
.bg_custom_main_footer{background-color:<?php echo $main_footer; ?>!important;}
<?php endif; ?>
<?php if (!empty($footer_text_color)): ?>
.bg_custom_main_footer {color:<?php echo $footer_text_color; ?>!important;}
<?php endif; ?>
<?php if (!empty($bg_secondary_footer)): ?>
.bg_custom_secondary_footer{background-color:<?php echo $bg_secondary_footer; ?>!important;}
<?php endif; ?>
<?php if (!empty($secondary_footer_text_color)): ?>
.bg_custom_secondary_footer {color:<?php echo $secondary_footer_text_color; ?>!important;}
<?php endif; ?>
<?php if (!empty($footer_and_sidebar_link_colors)): ?>
#sidebar a, .widget_footer a, .form.form-box a, .table a{color:<?php echo $footer_and_sidebar_link_colors; ?>!important;}
<?php endif;