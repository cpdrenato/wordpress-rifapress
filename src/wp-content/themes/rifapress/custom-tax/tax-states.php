<?php

add_action('init', 'register_my_tax_states', 10);

function register_my_tax_states() {
    register_taxonomy("states", array(
        0 => 'post',
            ), array(
        'hierarchical' => '0',
        'update_count_callback' => '_update_post_term_count',
        'rewrite' =>
        array(
            'slug' => 'estado',
            'with_front' => true,
            'hierarchical' => false,
        ),
        'query_var' => 'estados',
        'public' => false,
        'show_ui' => true,
        'show_tagcloud' => false,
        'labels' =>
        array(
            'name' => 'Estados',
            'singular_name' => 'Estado',
            'search_items' => 'Pesquisar estado',
            'popular_items' => 'Estados populares',
            'all_items' => 'Todos estados',
            'parent_item' => 'Estado pai',
            'parent_item_colon' => 'Estado pai',
            'edit_item' => 'Editar estado',
            'update_item' => 'Atualizar estado',
            'add_new_item' => 'Adicionar novo estado',
            'new_item_name' => 'Novo estado',
            'separate_items_with_commas' => 'Separe os estados com v�rgulas',
            'add_or_remove_items' => 'Adicionar ou remover estado',
            'choose_from_most_used' => 'Escolha entre o mais utilizado',
			'not_found' => 'Nenhum estado encontrado',
			'no_terms' => 'N�o h� estado',
        ),
        'capabilities' =>
        array(
            'manage_terms' => 'manage_categories',
            'edit_terms' => 'manage_categories',
            'delete_terms' => 'manage_categories',
            'assign_terms' => 'edit_posts',
        ),
        'show_in_nav_menus' => false,
    ));
}
