<?php

add_action('init', 'register_my_taxo_cidades', 10);

function register_my_taxo_cidades() {
    register_taxonomy("cidades", array(
        0 => 'post',
            ), array(
        'hierarchical' => '0',
        'update_count_callback' => '_update_post_term_count',
        'rewrite' =>
        array(
            'slug' => 'cidade',
            'with_front' => true,
            'hierarchical' => false,
        ),
        'query_var' => 'cidades',
        'public' => false,
        'show_ui' => true,
        'show_tagcloud' => false,
        'labels' =>
        array(
            'name' => 'Cidades',
            'singular_name' => 'cidade',
            'search_items' => 'Pesquisar cidade',
            'popular_items' => 'Cidades populares',
            'all_items' => 'Todas cidades',
            'parent_item' => 'Cidade pai',
            'parent_item_colon' => 'Cidade pai',
            'edit_item' => 'Editar cidade',
            'update_item' => 'Atualizar Cidade',
            'add_new_item' => 'Adicionar nova cidade',
            'new_item_name' => 'Nova cidade',
            'separate_items_with_commas' => 'Separe as cidades com v�rgulas',
            'add_or_remove_items' => 'Adicionar ou remover cidade',
            'choose_from_most_used' => 'Escolha entre o mais utilizado',
			'not_found' => 'Nenhuma cidade encontrada',
			'no_terms' => 'N�o h� cidade',
        ),
        'capabilities' =>
        array(
            'manage_terms' => 'manage_categories',
            'edit_terms' => 'manage_categories',
            'delete_terms' => 'manage_categories',
            'assign_terms' => 'edit_posts',
        ),
        'show_in_nav_menus' => false,
    ));
}
