<div class="container bg-menu-color">
<div class="search content">
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'rifapress' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Pesquisar', 'placeholder', 'rifapress' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'rifapress' ); ?></span></button>
</form>
<div class="clear"></div>
</div>
</div>