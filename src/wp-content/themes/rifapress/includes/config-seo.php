<?php global $wp; $current_url = home_url(add_query_arg(array(),$wp->request)); ?>
<meta name="robots" content="index, follow" />
<meta property="og:locale" content="pt_BR" />
<meta property="og:url" content="<?php echo $current_url;?>/" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
<meta property="og:title" content="<?php the_title(); ?>" />
<meta property="og:description" content="<?php bloginfo('description'); ?>" />