<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="UTF-8">
<title><?php echo $email_subject; ?></title>	
</head>
<body style="background:#F7F3F0; padding:20px; font-family:'Lucida Sans Unicode','Lucida Sans','Lucida Grande',sans-serif;">	
<div style="width:100%; max-width:600px; padding:0 20px 20px 20px; background:#fff; margin:0 auto; border-top: 7px solid #6c757d;
moz-border-radius:5px; -webkit-border-radus:5px; border-radius:5px; color:#454545;line-height:1.5em; " id="email_content">
<h1 style="padding-bottom:10px;font-weight:600;font-size:1.5em;color:#000;text-align:center;border-bottom:1px solid #eee;">
<?php echo $email_subject; ?>
</h1>