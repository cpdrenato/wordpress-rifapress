<?php
add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');

function wpdocs_set_html_mail_content_type()
{
    return 'text/html';
}

add_filter('wp_mail_from_name', 'ewp_ep_mail_from_name');

function ewp_ep_mail_from_name()
{
    return get_bloginfo('name');
}

add_action('save_post', 'rp_send_email_order');
function rp_send_email_order($post_id)
{
    if (get_post_type($post_id) !== 'post_product_order') {
        return;
    }
	
	if( is_admin() ) {
		
		return;
	}

    $blogname = get_bloginfo('name');

    $post = get_post($post_id);

    $client_name = get_field('client_name', $post_id);

    $edit = get_edit_post_link($post_id, '');

    $urlAdmin = admin_url('edit.php?post_type=post_product_order');

    $title = $post->post_title;

    $to = get_option('admin_email');

    $email_subject = 'Novo(s) número(s) reservado(s)';

    $link = get_permalink($post_id);

    ob_start();

    include get_template_directory() . '/includes/email_header.php';
    ?>

	<p>
	Olá,<br>
	Admin.
	<br>
	Novo(s) número(s) reservado(s), por <?php echo $client_name; ?>.
	<br>
	Título: <?php echo $title; ?><br>
	Para ver: <a href="<?php echo $urlAdmin; ?>">clique aqui</a>.
	</p>
	<?php
 include get_template_directory() . '/includes/email_footer_admin.php';

 $message = ob_get_contents();

 ob_end_clean();

 wp_mail($to, $email_subject, $message);
}
