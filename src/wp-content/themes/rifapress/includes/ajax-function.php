<?php
function get_city_search() {

    function query_group_by_filter($groupby) {
        global $wpdb;

        return $wpdb->postmeta . '.meta_value ';
    }

    add_filter('posts_groupby', 'query_group_by_filter');

    $getState = $_GET['state'];

    echo '<option value="" selected="" disabled="">' .__ ('Cidade', 'rifapress') . '</option>';

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'meta_key' => 'raffle_city',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'states',
                'field' => 'slug',
                'terms' => $getState
            ),
        ),

    );

    $query = new WP_Query($args);

    remove_filter('posts_groupby', 'query_group_by_filter');

    if ($query->have_posts()) :

        while ($query->have_posts()) : $query->the_post();
		

            $city = get_field('raffle_city');
		
            echo "<option value='$city->slug'> $city->name </option>";

        endwhile;

        wp_reset_postdata();

    else:

        echo '<option value="" selected="" disabled="">' . __('Não há cidades nesse estado', 'rifapress') . '</option>';

    endif;

    die();
}

add_action('wp_ajax_nopriv_get_city_search', 'get_city_search');
add_action('wp_ajax_get_city_search', 'get_city_search');

function pro_article_favorite() {
    $cookiename = "favorites";
    $favorites = array();
    $getId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
    if (!empty($getId)) {
        $id = $getId;
        $checkCookiename = json_decode($_COOKIE[$cookiename]);
        if (!empty($checkCookiename)) {
            $favorites = json_decode($_COOKIE[$cookiename]);
            $search = array_search($id, $favorites);
            if ($search === false) {
                array_push($favorites, $id);
                setcookie($cookiename, json_encode($favorites), time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
            } elseif ($search !== false) {
                array_splice($favorites, $search, 1);
                setcookie($cookiename, json_encode($favorites), time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
            }
        } else {
            array_push($favorites, $id);
            setcookie($cookiename, json_encode($favorites), time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
        }
    } else {

        echo 'Erro';
    }
    exit;
}

add_action('wp_ajax_nopriv_pro_article_favorite', 'pro_article_favorite');
add_action('wp_ajax_pro_article_favorite', 'pro_article_favorite');


	add_action('wp_ajax_nopriv_load_more', 'wp_load_more');
    add_action('wp_ajax_load_more', 'wp_load_more');
	
	function wp_load_more (){
		$paged = $_POST['page']+1;
		
		$query = new WP_Query( array(
		
		'post_type' => 'post',
		'post_status' => 'publish',
		'paged' => $paged,
		));
		
		if($query->have_posts()){ while($query->have_posts()){ $query->the_post();
		

				get_template_part('template-parts/home-content');
			
		}
		
        wp_reset_postdata();
		
		}else{
		
		$response = [
		 'msg' => '<p class="h4 text-center color_custom_title">Você chegou ao fim dos resultados.</p>'
		];
		wp_send_json_error( $response );
		
		}
		die();
		
	}