	<?php get_header(); ?>
	<div class="container">
	<div class="my-4 border-bottom">
	<h2 class="text-center text-uppercase h4"><?php the_title(); ?></h2>
	</div>
	<div class="row mb-5">
	<div class="col">
	<?php if (have_posts()): while (have_posts()): the_post(); ?>
	<?php endwhile; ?>
	<?php the_content(); ?>
	<?php else : ?>
	<p class="text-center text-uppercase">Nada encontrado</p>
	<?php endif; ?>
	</div>
	</div>
	</div>
	<?php get_footer();