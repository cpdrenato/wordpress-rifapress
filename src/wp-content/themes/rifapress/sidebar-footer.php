<?php
if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) || is_active_sidebar( 'sidebar-footer' )  ) : ?>

		<?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>

				<?php dynamic_sidebar( 'sidebar-footer' ); ?>
		<?php endif; ?>
		
<?php endif;