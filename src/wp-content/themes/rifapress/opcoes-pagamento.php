<?php
get_header(); 

	/*

	*

	* Template Name: Opções de Pagamento

	*

	*/

$dataPacked1 = str_replace("\\","",$_COOKIE["purchase_data"]);
$sessionData = unserialize(sanitize_text_field($dataPacked1));
	
if($sessionData) {
$pid = $sessionData['pid'];
$title = $sessionData['title'];
$vlitem = $sessionData['vlitem'];
$qtd = $sessionData['qtd'];
$order_code = $sessionData['orderid'];

$itens = $sessionData['itens'];

$whatsapp = get_field('number_whatsapp', 'option');
$phone = (!empty($whatsapp) ? $whatsapp : false);
$message = 'Gostaria de mais informações para realizar o pagamento do(s) número(s)'.$itens.' da Rifa '.$title.'. Obrigado(a).';
$urlencode = 'https://api.whatsapp.com/send?l=pt_br&phone='.urlencode($phone).'&text='.urlencode($message);


$enable_payment_gateway = get_field('enable_payment_gateway', 'option');
$paymentMethod = get_field('_payment_method', 'option');

$pagseguro_account_email = get_field('pagseguro_account_email', 'option');
$token = get_field('token_account_pagseguro', 'option');

$payment_show = get_field('_payment_show', 'option');



if(!empty($enable_payment_gateway) && !empty($pagseguro_account_email) && !empty($token)) {

$emailShop = $pagseguro_account_email;
$strpos = strpos($vlitem, '.');
$value = (!empty($strpos) ? $vlitem : $vlitem .'.00');

$url = 'https://ws.pagseguro.uol.com.br/v2/checkout';

$data = array(
'token' => $token,
'email' => $emailShop,
'currency' => 'BRL',
'itemId1' => $pid,
'itemQuantity1' => $qtd,
'itemDescription1' => $order_code. '-' .$title,
'itemAmount1' => $value,
'reference' => $order_code
);

$data = http_build_query($data);
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'));
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
$xml = curl_exec($curl);

if($xml == 'Unauthorized'){
	
echo "ERRO INTERNO: VERIFIQUE OS SEUS DADOS DO PAGSEGURO";

}

curl_close($curl);

$xml = simplexml_load_string($xml);

if(count($xml->error) > 0){

echo "ERRO INTERNO: NÃO FOI POSSÍVEL GERAR O BOTÃO DE PAGAMENTO";

}
	
}

$token_account_mercadopago = get_field('token_account_mercadopago', 'option');

if(!empty($enable_payment_gateway) && !empty($token_account_mercadopago)) {
	
	require_once get_template_directory() . '/vendor/autoload.php';
	
    MercadoPago\SDK::setAccessToken($token_account_mercadopago);
      
  $preference = new MercadoPago\Preference();
  
  $vlitem = str_replace(',', '.', $vlitem );
  $item = new MercadoPago\Item();
  $item->id = $pid;
  $item->title = $order_code. '#' .$title; 
  $item->quantity = $qtd;
  $item->unit_price = $vlitem;
  
  $preference->items = array($item);
  
  $preference->save();
  
   if($preference->error){
	  
    echo "ERRO INTERNO: VERIFIQUE OS SEUS DADOS DO MERCADOPAGO";
		
  }
      
}



?>

<div class="container">
<div class="my-4 border-bottom">
<h2 class="text-center text-uppercase h4">Opções de pagamento</h2>
</div>
<div class="row mb-5 ">
<?php if(!empty($enable_payment_gateway) && !empty($token_account_mercadopago) && ($payment_show == "0" OR $payment_show == "2")) { ?>
<div class="col-12 col-sm-6 col-md-4 my-3">
<img class="d-block w-100" width="" height="" src="<?php echo get_template_directory_uri() ;?>/img/mercado-pago.png">
	<a class="btn btn-lg d-block mt-2" href="<?php echo $preference->init_point; ?>" target="_blank" style="background-color:#009EE3; color:#fff;">
	<i class="fa fa-lock"></i> Pagar com Mercado Pago</a>
</div>
<?php } if(!empty($enable_payment_gateway) && !empty($pagseguro_account_email) && !empty($token) && ($payment_show == "1" OR $payment_show == "2")) { ?>
<div class="col-12 col-sm-6 col-md-4 my-3">
<img class="d-block w-100" width="" height="" src="<?php echo get_template_directory_uri() ;?>/img/pagseguro.png">
<a class="btn btn-lg d-block mt-2" href="<?php echo 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml->code; ?>" target="_blank" style="background-color:#df7704; color:#fff;"><i class="fa fa-lock"></i> Pagar com PagSeguro</a>
</div>
<?php } if(!empty($phone)) { ?>

<div class="col-12 col-sm-6 col-md-4 my-3 m-auto">
<img class="d-block w-100" src="<?php echo get_template_directory_uri() ;?>/img/whatsapp.png">
<a class="btn btn-lg d-block mt-2" href="<?php echo $urlencode; ?>" target="_blank" style="background-color:#00ac4e; color:#fff;"><i class="fa fa-lock"></i> Pagar com WhatsApp</a>
</div>
<?php } ?>
</div>

<?php	$payment_data = get_field('_payment_data', $pid);  ?>



	<?php if($payment_data){ ?>
	<?php if( have_rows('payment_content', $pid) ){ ?>

	<div class="row border-top">
	<?php while ( have_rows('payment_content', $pid) ) { the_row(); 
	$payment_image = get_sub_field('add_payment_option_image');
	$payment_title = get_sub_field('add_payment_title');
	$text_field = get_sub_field('_add_text_field');
	?>
	<div class="col-12 col-sm-6 col-md-3 col-lg-2 my-3">
	<div class="mb-2"><img class="d-block w-100" src="<?php echo $payment_image ?>"></div>
	<p class="text-center"><b><?php echo $payment_title ?></b></p>
	<?php if( !empty($text_field) && have_rows('_add_text_field') ){ 
	while ( have_rows('_add_text_field') ) { the_row(); 
	$payment_option_text = get_sub_field('payment_option_text'); ?>
	<p class="text-center"><?php echo $payment_option_text; ?></p>
	<?php } } ?>
	</div>
	<?php } ?>
	</div>

	<?php } } ?>

	</div>
<?php
 }else{ 
 wp_redirect( home_url() ); exit; } 
get_footer();
?>