<div class="container-fluid bg-success py-1 mb-3 bg_custom_filter">
<form class="container" action="<?php echo esc_url(home_url('/')); ?>">
            <input type="hidden" id="rifa" name="rifa" value="pesquisa">
            <input type="hidden" value=""  name="s" id="name" />
    <div class="form-row">
        <div class="col-12 col-sm-6 col-md-3 my-1">
            <select class="custom-select custom-select-lg" name="tipo" id="tipo_id">
                    <option value="" selected="selected"  disabled="disabled"><?php _e('Categoria'); ?></option>
                    <?php
                    $categories = get_categories();
                    if ($categories):
                        foreach ($categories as $category):
                            ?>
                            <option value="<?php echo $category->slug; ?>"><?php echo $category->cat_name; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?> 
            </select>
        </div>
        <div class="col-12 col-sm-6 col-md-3 my-1">
<?php $terms = get_terms('states', array('hide_empty' => true,)); ?>
            <select class="custom-select custom-select-lg" name="state" id="state_id">
                    <option value="" disabled="disabled" selected><?php _e('Estado'); ?></option>
                    <?php
                    if ($terms):
                        foreach ($terms as $term):
                            echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
                        endforeach;
                    endif;
                    ?>
            </select>
        </div>
        <div class="col-12 col-sm-6 col-md-3 my-1">
            <select name="city" class="custom-select custom-select-lg" id="city_id">
                    <option value="" disabled="disabled" selected><?php _e('Cidade'); ?></option>
                    <option value="" disabled="disabled"><?php _e('Selecione um estado'); ?></option>
            </select>
        </div>
        <div class="col-12 col-sm-6 col-md-3 my-1">
			 <input type="submit" class="btn btn-dark form-control btn-lg form-control-lg" value="Pesquisar"/>
        </div>
    </div>
</form>
</div>