# Wordpress + Rifapress

> O tema funciona no PHP 5.6 ou superior.
> Versão suportada do WordPress 5.0 ou superior.

Username: admin
Password : YeN39Xy$uuPyeE)%dfH5MvdE
Email: cpdrenato@gmail.com

### Requirements

- docker
- docker-compose
- linux shell (Make wrapper used to boot and run project tasks)

## Start stack

```
make up
```

## Listen to logs

```
make tail
```

## Shut it down

```
make down
```

## Detalhes
- All in One Wp - All-in-One WP Migration
- Editor clássico
- Contact Form 7
- Disable XML-RPC
- Importador do WordPress
- Limit Login Attempts Reloaded
- Mercado Pago
- PagSeguro